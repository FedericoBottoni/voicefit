# VoiceFit

> VoiceFit is an app designed for a suitable gym, stakeholder of the project too, which first frontend is developed as an Amazon Alexa Skill, a second frontend as a webapp and a backend consisting of a database, API for the communication with Alexa and with the webapp interfaces. The just described architecture would be used by the the gym personal trainers and by clients: the former can use the webapp to add ad hoc "free body" training cards for customers, while they can use Alexa's skills to get the suggested daily exercises and to perform them using the interactions with the device.

## Project structure

The application consists in different modules wired to each other. The users are splitted in two roles: the personal trainers and the customers. The firsts interact to the system through an user-friendly web applictaion capable of handling the customer's sheets (create, update and delete) and assigning these to the available clients; the seconds can interact to Voicefit thanks to their own Alexa device, asking her for their daily sheet and being led into the related exercises.
Both the user interface systems get across to the common backend in order to interact with the same database.

![Project structure diagram](img/diagram.png "Project structure")


## Technologies
### Frontend webapp 
* [react.js](http://reactjs.org/) framework
* [material-ui](https://material-ui.com/) style library
* [node.js](https://nodejs.org/) web server
* [axios](https://github.com/axios/axios) to send http requests

### Frontend alexa-skill 
* [django-ask-sdk](https://github.com/alexa/alexa-skills-kit-sdk-for-python/tree/master/django-ask-sdk)
* [alexa-skills-kit](https://developer.amazon.com/en-US/alexa/alexa-skills-kit)

### Backend server
* [django](https://www.djangoproject.com/) framework to handle http requests and db queries
* [django-ask-sdk](https://github.com/alexa/alexa-skills-kit-sdk-for-python) package to connect and handle Alexa’s calls
* [python vanilla](https://www.python.org/) used to build the cache system
* [mariadb](https://mariadb.org/) opensource sql database

### Backend technologies
* [raspberry pi](https://www.raspberrypi.org/) hosting HW (v1 model B)
* [raspbian](https://www.raspberrypi.org/downloads/raspbian/) operating system (v4.19.97)

## Skill flow

The voicefit skill allows the customers to get into their daily sheet, driving them to the completation of each exercise, serie by serie and giving the possibility to skip the exercise they are not able to finish it. The flow describes the finite states automata of the voice application.

![Skill flow diagram](img/VoiceFit_skill_flow.png "Skill flow")


## DB structure

The architecture describes how the relationships between the application's entiteis are built.

![DB structure diagram](img/DB_schema.png "DB structure")

## DevOps architecture

![DevOps architecture](img/Tech_Diagram.png "DevOps architecture")



## Team

**Mattia Artifoni** [807466]:
[@m-artifoni](https://gitlab.com/m-artifoni)

**Federico Bottoni** [806944]:
[@FedericoBottoni](https://gitlab.com/FedericoBottoni)

**Riccardo Capra** [808227]:
[@riccardocapra](https://gitlab.com/riccardocapra)

**Davide Samotti** [807555]:
[@davide_samo](https://gitlab.com/davide_samo)
