#!/bin/bash

INITENV=$1
BRANCH=$2
STAGE=$3
PASSWD=$4
CUSTOMCOM=$5
INITCOM="screen -dm -S ${INITENV}_${STAGE} -T vt100 && screen -S ${INITENV}_${STAGE} -T vt100 -X multiuser on && screen -S ${INITENV}_${STAGE} -T vt100 -X acladd root && screen -S ${INITENV}_${STAGE} -T vt100 -X stuff $'cd ~/${INITENV}_${BRANCH} && "
COM="${INITCOM}${CUSTOMCOM}\n'"
echo "Executing commands inside ${INITENV}_${BRANCH} directory on deploy server"
sshpass -p "$PASSWD" ssh -tt -o StrictHostKeyChecking=no deployBot@voicefit.tk "$COM"