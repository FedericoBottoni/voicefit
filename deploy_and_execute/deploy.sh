#!/bin/bash

INITENV=$1
RSCREENS=$2
BRANCH=$3
STAGE=$4
PASSWD=$5
FILES=$6

echo "Stopping active sessions and creating ${INITENV}_${BRANCH} directory on deploy server"

for ENTRY in $RSCREENS
do
    echo "Closing session: ${INITENV}_${ENTRY}"
    sshpass -p "$PASSWD" ssh -tt -o StrictHostKeyChecking=no deployBot@voicefit.tk "screen -S ${INITENV}_${ENTRY} -T vt100 -X quit"
done

sshpass -p "$PASSWD" ssh -tt -o StrictHostKeyChecking=no deployBot@voicefit.tk "cd ~ && rm -rf ${INITENV}_${BRANCH} && mkdir -p ${INITENV}_${BRANCH}"
for ENTRY in $FILES
do
    if [[ ${ENTRY:0:1} == "." ]];
    then
        echo "Skipping hidden entry: ${ENTRY}"
    else
        echo "Uploading entry: ${ENTRY}"
        if [[ -d $ENTRY ]]
        then
            sshpass -p "$PASSWD" scp -o StrictHostKeyChecking=no -rp "$ENTRY" deployBot@voicefit.tk:"~/${INITENV}_${BRANCH}"
        else
            sshpass -p "$PASSWD" scp -o StrictHostKeyChecking=no -p "$ENTRY" deployBot@voicefit.tk:"~/${INITENV}_${BRANCH}"
        fi
    fi
done

echo "Setting right permissions on ${INITENV}_${BRANCH} remote directory"
sshpass -p $PASSWD ssh -tt -o StrictHostKeyChecking=no deployBot@voicefit.tk "cd ~/.. && sudo chmod -R ugo+rwx deployBot"