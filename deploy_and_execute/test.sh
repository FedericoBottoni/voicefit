#!/bin/bash

INITENV=$1
BRANCH=$2
PASSWD=$3
COM=$4

echo "Executing tests inside ${INITENV}_${BRANCH} directory on deploy server"
sshpass -p "$PASSWD" ssh -tt -o StrictHostKeyChecking=no deployBot@voicefit.tk "$COM"

sshpass -p "$PASSWD" scp -o StrictHostKeyChecking=no -p deployBot@voicefit.tk:"~/${INITENV}_${BRANCH}/api/back-test.log" .
OUTPUT=$(cat "./back-test.log")
case $OUTPUT in
    *exception*)
        exit 1
        ;;
esac

case $OUTPUT in
    *Exception*)
        exit 1
        ;;
esac

case $OUTPUT in
    *EXCEPTION*)
        exit 1
        ;;
esac

case $OUTPUT in
    *error*)
        exit 1
        ;;
esac

case $OUTPUT in
    *Error*)
        exit 1
        ;;
esac

case $OUTPUT in
    *ERROR*)
        exit 1
        ;;
esac