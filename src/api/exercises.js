import API from './api';

/**
 * Allows to gather every information about the existing exercises
 * @kind module
 */
export const getExercises = () => API.get(`exercise/getall`);

