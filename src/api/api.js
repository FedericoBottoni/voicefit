import axios from 'axios';
import { apiBaseURL } from '../config'

export default axios.create({
  baseURL: apiBaseURL
});