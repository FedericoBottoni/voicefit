import API from './api';

/**
 * Allows to gather every information about the existing sheets
 * @kind module
 */
export const getSheets = () => API.get(`sheet/getall`);

/**
 * Perform the assignation of a customers set, in the specified dates, to the specified sheet
 * @kind module
 */
export const assignSheet = (body) => API.post(`sheet/assign`, body);

/**
 * Create a new sheet from the passed object
 * @kind module
 */
export const createSheet = (body) => API.post(`sheet/create`, body);

/**
 * Updated the specified sheet from the passed object
 * @kind module
 */
export const updateSheet = (body) => API.put(`sheet/update`, body);

/**
 * Delete a sheet from its id
 * @kind module
 */
export const deleteSheet = (sheetId) => API.delete(`sheet/delete/${sheetId}`);
