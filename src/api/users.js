import API from './api';

/**
 * Gather each customer
 * @kind module
 */
export const getCustomers = () => API.get(`customer/getall`);

/**
 * Gather customers filtering the availables in the passed dates set
 * @kind module
 */
export const getAvailableCustomers = (body) => API.post(`customer/getavailable`, body);
