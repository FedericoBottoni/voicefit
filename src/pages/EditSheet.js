import React, { useState, useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import AddIcon from '@material-ui/icons/Add';
import CreateIcon from '@material-ui/icons/Create';
import DeleteIcon from '@material-ui/icons/Delete';
import { Avatar, Fab, List, ListItem, ListItemIcon, ListItemText,
   IconButton, ListItemSecondaryAction, Paper, CircularProgress } from '@material-ui/core';
import DeleteConfirmDialog from '../components/dialogs/DeleteConfirmDialog'
import { getSheets, deleteSheet } from '../api/sheet';
import CreateSheet from '../components/dialogs/CreateSheet';
import sheetStub from '../data/SheetsSchema.json';
import { useStub } from '../config';

const useStyles = makeStyles((theme) => ({
  title: {
    display: 'flex',
    flexDirection: 'row',
    width: '100%',
  },
  root: {
    position: 'relative',
    paddingBottom: 88,
  },
  fab: {
    position: 'absolute',
    bottom: 16,
    right: 16,
  },
  wrapper: {
    margin: theme.spacing(1),
    position: 'relative',
    width: '100%',
    height: 'calc(100vh - 270px)'
  },
  elementProgress: {
    position: 'absolute',
    top: '50%',
    left: '50%',
    marginTop: -12,
    marginLeft: -12,
  },
}));

const EditSheet = ({ setDefaultSnackbarError }) => {
  const classes = useStyles();
  const [sheets, setSheets] = useState(null);
  const [createOpen, setCreateOpen] = useState(false);
  const [sheetToDelete, setSheetToDelete] = useState(null);
  const [loading, setLoading] = useState(false);
  const [sheetLoading, setSheetLoading] = useState(false);

  const initLists = () => {
    setSheetLoading(true);
      (useStub ? Promise.resolve({ data: sheetStub }) : getSheets())
        .then(res => {
          setSheets(res.data);
          setSheetLoading(false);
        })
        .catch(() => {
          setDefaultSnackbarError();
          setSheetLoading(false);
        })
      }
    
  const onCloseDeleteDialog = () => {
    setSheetToDelete(null);
  }
    
  const onDeleteConfirm = () => {
    const { id } = sheetToDelete;
    setLoading(true);
    deleteSheet(id)
    .then(res => {
      setSheets(res.data);
      onCloseDeleteDialog();
      setLoading(false);
    })
    .catch(() => {
      setDefaultSnackbarError();
      setLoading(false);
    })
  }

  useEffect(() => {
    if(!createOpen && !sheetToDelete) {
      initLists()
    }
  }, [createOpen, sheetToDelete]);
  

  return (<div className={classes.root}>
    <CreateSheet
      setDefaultSnackbarError={setDefaultSnackbarError}
      createOpen={createOpen}
      onDialogClose={() => setCreateOpen(false)}
    />
    <DeleteConfirmDialog onClose={onCloseDeleteDialog} sheetToDelete={sheetToDelete} onDeleteConfirm={onDeleteConfirm} loading={loading} setLoading={setLoading} />
    <div className={classes.title}>
      <div>
        <h1>Gestisci schede</h1>
      </div>
    </div>
    {sheets && !sheetLoading ? <>
      <Paper>
        <List>
          {sheets.map(sheet => <ListItem>
            <ListItemIcon>
              <Avatar>{sheet.name[0]}</Avatar>
            </ListItemIcon>
            <ListItemText primary={sheet.name} />
            <ListItemSecondaryAction>
              <IconButton onClick={() => setCreateOpen(sheet)} aria-label="edit">
                <CreateIcon />
              </IconButton>
              <IconButton onClick={() => setSheetToDelete(sheet)} aria-label="delete">
                <DeleteIcon />
              </IconButton>
            </ListItemSecondaryAction>
          </ListItem>)}
        </List>
      </Paper>
        <Fab
          className={classes.fab}
          color="primary"
        >
            <IconButton 
              onClick={() => setCreateOpen(true)}
            >
              <AddIcon style={{ color: '#FFF' }} />
            </IconButton>
        </Fab>
      </> :
      <div className={classes.wrapper}>
        {sheetLoading && <CircularProgress size={32} className={classes.elementProgress} />}
      </div>}
    
  </div>);
}

export default EditSheet;
