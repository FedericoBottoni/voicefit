import React, { useState, useEffect } from 'react';
import moment from 'moment'
import { makeStyles } from '@material-ui/core/styles';
import { KeyboardDatePicker } from '@material-ui/pickers';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import CircularProgress from '@material-ui/core/CircularProgress';
import Checkbox from '@material-ui/core/Checkbox';
import Button from '@material-ui/core/Button';
import TransferList from '../components/TransferList';
import { getAvailableCustomers } from '../api/users';
import { assignSheet } from '../api/sheet';
import customersStub from '../data/userData.json'
import SheetList from '../components/SheetList';
import SheetDialog from '../components/dialogs/SheetDialog';
import { useStub } from '../config';

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    flexDirection: 'column',
    width: '100%',
    height: 'calc(100% - 48px)'
  },
  container: {
    display: 'flex',
    flexDirection: 'row',
    padding: '16px 48px',
    height: 'calc(100% - 160px)'
  },
  datesContainer: {
    display: 'flex',
    flexDirection: 'column',
    minWidth: 576,
    width: '42%'
  },
  dates: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  weekdays: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  includeLabel: {
    paddingTop: 16,
    color: 'rgba(0, 0, 0, 0.54)',
    fontFamily: '"Roboto", "Helvetica", "Arial", sans-serif',
    fontWeight: 400,
    letterSpacing: '0.00938em',
  },
  customerContainer: {
    paddingTop: 24 
  },
  leftContainer: {
    minWidth: '50%'
  },
  rightContainer: {
    display: 'flex',
    flex: 1,
    paddingLeft: 16
  },
  submit: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'flex-end',
    padding: '16px 48px',
  },
  wrapper: {
    margin: theme.spacing(1),
    position: 'relative',
  },
  elementProgress: {
    position: 'absolute',
    top: '50%',
    left: '50%',
    marginTop: -12,
    marginLeft: -12,
  }
}));

const ERROR_MSG = {
  MIN_DATE: "Data non valida",
  INVALID_DATE: "Data non valida"
}

const WEEKDAYS = {
  MONDAY: { id: 1, name: 'Lun'},
  TUESDAY: { id: 2, name: 'Mar'},
  WEDNESDAY: { id: 3, name: 'Mer'},
  THURSDAY: { id: 4, name: 'Gio'},
  FRIDAY: { id: 5, name: 'Ven'},
  SATURDAY: { id: 6, name: 'Sab'},
  SUNDAY: { id: 0, name: 'Dom'},
}

const ignoreTimeZone = (dates) => 
  dates.map(d => {
    const date = new Date(d);
    return new Date(date.setMinutes(date.getMinutes() - (new Date()).getTimezoneOffset())).toISOString()
  });


const onWeekDayChange = (id, checked, selectedWeekdays, setSelectedWeekdays) => {
  let nextSelectedWeekdays;
  if(checked && selectedWeekdays.indexOf(id) === -1) {
    nextSelectedWeekdays = [...selectedWeekdays, id];
  } else if(!checked && selectedWeekdays.indexOf(id) !== -1) {
    nextSelectedWeekdays = selectedWeekdays.filter(x => x !== id);
  }
  setSelectedWeekdays(nextSelectedWeekdays);
}

const filterByWeekdays = (includedDays, selDay) => {
  return includedDays.indexOf(selDay.day()) !== -1;
}

const enumerateDaysBetweenDates = (startDateRef, endDateRef, filterDay) => {
  let startDate = startDateRef.clone();
  let endDate = endDateRef.clone();
  if(startDateRef.isBefore(endDateRef)) {
    const dates = filterDay(startDate) ? [startDate.toDate()] : [];
    startDate = startDate.add(1, 'days');

    while(startDate.format('M/D/YYYY') !== endDate.format('M/D/YYYY')) {
      if(filterDay(startDate)) {
        dates.push(startDate.toDate());
      }
      startDate = startDate.add(1, 'days');
    }
    if(filterDay(endDate)) {
      dates.push(endDate.toDate());
    }
    return dates;
  } else if(startDateRef.isSame(endDateRef)) {
    return [endDate.toDate()];
  } else {
    return [];
  }
};

export default function AssignSheet({ setDefaultSnackbarError }) {
  const TODAY = moment();
  const classes = useStyles();
  TODAY.startOf('day');
  const [fromDate, setFromDate] = useState(TODAY);
  const [toDate, setToDate] = useState(TODAY);
  const [selectedWeekdays, setSelectedWeekdays] = useState(Object.keys(WEEKDAYS).map(dayKey => WEEKDAYS[dayKey].id));
  const [customers, setCustomers] = useState([]);
  const [selectedCustomers, setSelectedCustomers] = useState([]);
  const [selectedSheet, setSelectedSheet] = useState(null);
  const [selectedSheetInfo, setSelectedSheetInfo] = useState(null);
  const [loading, setLoading] = useState(false);
  const [customersLoading, setCustomersLoading] = useState(false);
  const handleClose = () => setSelectedSheetInfo(null);

  const filterByWeekdaysBound = (...params) => filterByWeekdays(selectedWeekdays, ...params);
  const selectedDates = enumerateDaysBetweenDates(fromDate, toDate, filterByWeekdaysBound);

  const onSubmit = () => {
    const dates = enumerateDaysBetweenDates(fromDate, toDate, filterByWeekdaysBound);
    const body = {
      customerIds: selectedCustomers.map(x => x.id),
      sheetId: selectedSheet,
      dates: ignoreTimeZone(dates)
    };

    setLoading(true);

    assignSheet(body)
    .then(res => {
        resetForm();
        setLoading(false);
        getCustomersList();
    })
    .catch(() => {
      setLoading(false);
      setDefaultSnackbarError();
    })
  };

  const resetForm = () => {
    setSelectedCustomers([]);
    setSelectedSheet(null);
    setFromDate(TODAY);
    setToDate(TODAY);
    setSelectedWeekdays(Object.keys(WEEKDAYS).map(dayKey => WEEKDAYS[dayKey].id));
  };

  const getCustomersList = () => {
    if(selectedDates && selectedDates.length > 0) {
      setCustomersLoading(true);
      (useStub ? Promise.resolve({data: customersStub}) : getAvailableCustomers({ dates: ignoreTimeZone(selectedDates) }))
        .then(res => {
          setCustomers(res.data);
          setCustomersLoading(false);
        })
        .catch(() => {
          setDefaultSnackbarError();
          setCustomersLoading(false);
        })
    }
  }

  useEffect(() => {   
    getCustomersList();
  }, [fromDate, toDate, selectedWeekdays]);

  useEffect(() => {
    const nextSelectedCustomers = selectedCustomers.map(x => customers.find(c => c.id === x.id)).filter(x => !!x);
    setSelectedCustomers(nextSelectedCustomers);
  }, [customers])

  const customersListCallback = (_, right) => {
    const nextSelectedCustomers = right.map(x => customers.find(c => `${c.surname} ${c.name}` === x)).filter(x => !!x);
    setSelectedCustomers(nextSelectedCustomers);
  }
  
  return (
  <div className={classes.root}>
    <SheetDialog sheet={selectedSheetInfo} onClose={handleClose} />
  <div>
    <h1>Assegna scheda</h1>
  </div>
    <div className={classes.container}>
      <div className={classes.leftContainer}>
        <div className={classes.datesContainer}>
          <div className={classes.dates}>
            <KeyboardDatePicker
              disableToolbar
              variant="inline"
              format="DD/MM/yyyy" // "DD/MM/yyyy dddd"
              inputVariant="outlined"
              disablePast
              minDateMessage={ERROR_MSG.MIN_DATE}
              invalidDateMessage={ERROR_MSG.INVALID_DATE}
              id="date-from"
              label="Inizio periodo"
              value={fromDate}
              onChange={(d) => {
                d.startOf('day')
                setFromDate(d);
              }}
              autoOk
            />
            <KeyboardDatePicker
            disableToolbar
            variant="inline"
            format="DD/MM/yyyy" // "DD/MM/yyyy dddd"
            inputVariant="outlined"
            disablePast
            minDate={fromDate}
            minDateMessage={ERROR_MSG.MIN_DATE}
            invalidDateMessage={ERROR_MSG.INVALID_DATE}
            id="date-to"
            label="Fine periodo"
            value={toDate}
            onChange={(d) => {
              d.startOf('day')
              setToDate(d);
            }}
            autoOk
          />
          </div>
          <div className={classes.includeLabel}>
            Giorni da includere
          </div>
          <div className={classes.weekdays}>
            {Object.keys(WEEKDAYS).map(dayKey => {
              const day = WEEKDAYS[dayKey];
              return <FormControlLabel
                control={
                  <Checkbox
                    checked={selectedWeekdays.indexOf(day.id) !== -1}
                    onChange={(e) => onWeekDayChange(day.id, e.target.checked, selectedWeekdays, setSelectedWeekdays)}
                    name={day.name}
                    color="primary"
                  />
                }
                label={day.name}
              />
            })}
          </div>
        </div>
        <div className={classes.customerContainer}>
          <TransferList
            title="Clienti"
            initLeft={customers.filter(c => selectedCustomers.map(x => x.id).indexOf(c.id) === -1).map(c => `${c.surname} ${c.name}`)}
            initRight={selectedCustomers.map(c => `${c.surname} ${c.name}`)}
            getStateCallback={customersListCallback}
            loading={customersLoading}
          />
        </div>
      </div>
      <div className={classes.rightContainer}>
        <SheetList
          setDefaultSnackbarError={setDefaultSnackbarError}
          setSelectedSheetInfo={setSelectedSheetInfo}
          selectedSheet={selectedSheet}
          setSelectedSheet={setSelectedSheet}
        />
      </div>
    </div>
    <div className={classes.submit}>
      <div className={classes.wrapper}>
        <Button
          disabled={loading || selectedCustomers.length === 0 ||
            selectedSheet === null ||
            selectedDates.length === 0}
          size="large"
          onClick={onSubmit}>
          Invia
        </Button>
        {loading && <CircularProgress size={24} className={classes.elementProgress} />}
      </div>
    </div>
  </div>)
}
