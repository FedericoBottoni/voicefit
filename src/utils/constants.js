/**
 * Default error message for API calls
 * @constant
 */
export const DEFAULT_ERROR_MSG = 'C\'è stato un errore nella funzione richiesta';