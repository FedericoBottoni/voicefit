import React, { useState, useEffect, useCallback } from 'react';
import { DndProvider } from 'react-dnd';
import Backend from 'react-dnd-html5-backend';
import { makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import CircularProgress from '@material-ui/core/CircularProgress';
import ExercisePaper from '../components/ExercisePaper.js';
import { createSheet, updateSheet } from '../api/sheet';
import { uuidv4 } from '../utils/guid';

const DEFAULT_REPS = 10;
const DEFAULT_COOLDOWN = 60;
const ItemTypes = {
  EXERCISE: 'EXERCISE'
}

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    flexDirection: 'row',
    flexGrow: 1,
    overflow: 'hidden',
    flexWrap: 'wrap'
  },
  button: {
    display: 'flex',
    justifyContent: 'flex-end',
    paddingTop: 8
  }, 
  wrapper: {
    margin: theme.spacing(1),
    position: 'relative',
  },
  buttonProgress: {
    position: 'absolute',
    top: '50%',
    left: '50%',
    marginTop: -12,
    marginLeft: -12,
  },
}));

const handleSubmit = ({ sheetName, duration, exercises, series, setDefaultSnackbarError, resetState, onClose, createMod, sheetId, setLoading }) => {
  let exercisesIds = [];
  let repetitions = [];
  let cooldowns = [];
  exercises.forEach(({ id }, i) => {
    const currentSeries = series.filter(x => x.exercise === id).sort((x,y) => x.order - y.order);
    repetitions[i] = currentSeries.map(x => x.reps).join(' ');
    cooldowns[i] = currentSeries.map(x => x.cd).join(' ');
    exercisesIds[i] = id;
  });
  const order = [...new Array(exercisesIds.length)].map((_, i) => i+1);
  let body = {
    sheetId,
    sheet_name : sheetName,
    duration : duration,
    order,
    exercise_schema_id: exercisesIds,
    repetitions,
    cooldowns,
    notes : [...(new Array(exercisesIds.length))].map(() => "")
  }
  
  const submitHandler = createMod ? createSheet : updateSheet;
  setLoading(true);
  
  submitHandler(body)
    .then(() => {
      onClose();
      resetState();
      setLoading(false);
    })
    .catch(err => {
      setLoading(false);
      setDefaultSnackbarError();
    })
}

const deleteSerie = (serieToDelete, series, setSeries) => {
  const nextSeries = series.filter(x => !(x.id === serieToDelete.id && x.exercise === serieToDelete.exercise));
  setSeries(nextSeries);
}

const addSerie = (exerciseToAdd, series, setSeries) => {
  const maxOrder = series.reduce((acc,curr) => acc > curr.order ? acc : curr.order, 0);
  const nextSeries = [ ...series, {
    exercise: exerciseToAdd.id,
    id: uuidv4(),
    reps: DEFAULT_REPS,
    cd: DEFAULT_COOLDOWN,
    order: maxOrder + 1 
  }]
  setSeries(nextSeries);
}

const array_move = (arr_imm, old_index, new_index) => {
  const arr = arr_imm.slice()
  if (new_index >= arr.length) {
      var k = new_index - arr.length + 1;
      while (k--) {
          arr.push(undefined);
      }
  }
  arr.splice(new_index, 0, arr.splice(old_index, 1)[0]);
  return arr;
};

const onDrop = (setExercises, exercises, {id, index}) => {
  const nextExercises = array_move(exercises, exercises.findIndex(x => x.id === id), index);
  setExercises(nextExercises);
}
/**
* This component manages the creation of a new sheets
 * @param    exercises Object that contains the list of exercises 
 * @param    sheetName Object that contains the name of the sheet 
 * @param    duration Object that contains the duration of the sheet 
 * @param    resetPageState Callback that triggers the reset of the page state
 * @param    setDefaultSnackbarError Callback which fires the snackbar error message
 * @param    setExercises Used to set the Exercises in case of a Drop event
 * @param    onClose Handler of the Close event
 */
const ExerciseDetails = ({ exercises, sheetName, duration, resetPageState, setDefaultSnackbarError, setExercises, onClose, createMod, sheetId }) => {
  const classes = useStyles();
  const [series, setSeries] = useState([]);
  const [dndBlankSpace, setDndBlankSpace] = useState(null);
  const [dragItem, setDragItem] = useState(null);
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    if(dragItem === null) {
      setDndBlankSpace(null);
    }
  }, [dragItem]);

  useEffect(() => {
    const nextSeries = series.filter(x => exercises.map(y => y.id).indexOf(x.exercise) !== -1);
    const newExercises = exercises.filter(x => nextSeries.map(y => y.exercise).indexOf(x.id) === -1);
    const newSeries = newExercises.map(ex => {
      let res;
      if(!ex.repetitions) {
        res = ([{
          exercise: ex.schema_id || ex.id,
          id: uuidv4(),
          reps: DEFAULT_REPS,
          cd: DEFAULT_COOLDOWN,
          order: 0
        }]);
      } else {
        const reps = ex.repetitions.split(' ');
        const coold = ex.cooldown.split(' ');
        res = [...new Array(reps.length)].map((_, i) => ({
          exercise: ex.schema_id || ex.id,
          id: uuidv4(),
          reps: reps[i],
          cd: coold[i],
          order: i
        }));
      }
      return res;
    });
    setSeries([...nextSeries, ...newSeries.flat()]);
  }, [exercises]);

  const setSerie = (serieObj) => {
    const nextSeries = series.filter(x => !(x.id === serieObj.id && x.exercise === serieObj.exercise));
    setSeries([...nextSeries, serieObj ])
  }

  const resetState = () => {
    resetPageState();
    setSeries([]);
  }
  const moveCard = useCallback(
    (dragIndex, hoverIndex) => {
      setDndBlankSpace(hoverIndex);
    },
    [exercises],
  );
  const exercisesToView = dndBlankSpace === null || !dragItem ?
    exercises :
    [...(new Array(exercises.length))]
      .map((_, i) => {
        const otherExercises = exercises.filter(x => x.id !== dragItem.id);
        let res;
        if(i < dndBlankSpace) {
          res = otherExercises[i];
        } else if ( i === dndBlankSpace) {
          res = {};
        } else {
          res = otherExercises[i-1];
        }
        return res;
      });
      
  return (<DndProvider backend={Backend}>
      <div className={classes.root}>
        {exercisesToView.map((exercise, index) =>
          <ExercisePaper
            ItemTypes={ItemTypes}
            series={series}
            setSeries={setSeries}
            setSerie={setSerie}
            addSerie={addSerie}
            deleteSerie={deleteSerie}
            exercise={exercise}
            index={index}
            dndBlankSpace={dndBlankSpace}
            moveCard={moveCard}
            setDragItem={setDragItem}
            dragItem={dragItem}
            onDrop={(...params) => onDrop(setExercises, exercises, ...params)}
          />)}
      </div>
      <div className={classes.button}>
      <Button
        color="secondary"
        onClick={onClose}>
        Annulla
      </Button>
      <div className={classes.wrapper}>
        <Button 
          disabled={loading || !sheetName || !exercises || exercises.length === 0 }
          onClick={() => handleSubmit({ sheetName, duration, exercises, series, setDefaultSnackbarError, resetState, onClose, createMod, sheetId, setLoading })}
        >
          Invia
        </Button>
        {loading && <CircularProgress size={24} className={classes.buttonProgress} />}
      </div>
    </div>
  </DndProvider>
  );
}

export default ExerciseDetails