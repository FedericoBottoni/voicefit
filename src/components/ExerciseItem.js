import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import FitnessCenter from '@material-ui/icons/FitnessCenter';
import Hotel from '@material-ui/icons/Hotel';

const useStyles = makeStyles((theme) => ({
  reps: {
    padding: '4px 16px',
    display: 'flex'
  },
  cooldowns: {
    padding: '4px 16px',
    display: 'flex'
  },
  listItemText: {
    display: 'flex',
    flexDirection: 'column'
  },
  listItemTextRow: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  icon: {
    marginRight: 6
  }
}));
/**
* This component renders the name of the exercise and the sliders
 * @param    index Object that constain the index of the selected exercise 
 * @param    schema Object that constain the name of the selected exercise 
 * @param    repetitionsSplitted Object that contains the repetitions of the exercises after they have been splitted
 * @param    cooldownsSplitted Object that contains the cooldowns of the exercises after they have been splitted
 */
const ExerciseItem = ({ index, schema, repetitionsSplitted, cooldownsSplitted }) => {
  const classes = useStyles();

  return (<ListItem>
    <ListItemText
        primary={<>{index+1}: {schema}</>}
        secondary={
        repetitionsSplitted.map((_, i) => <div className={classes.listItemText}>
        <div className={classes.listItemTextRow}>
            <span className={classes.reps}>
            <FitnessCenter className={classes.icon} style={i === 0 ? { visibility: 'visible' } : { visibility: 'hidden' }  } />{repetitionsSplitted[i]} reps
            </span>
            <span className={classes.cooldowns}>
            <Hotel className={classes.icon} style={i === 0 ? { visibility: 'visible' } : { visibility: 'hidden' }  } />{cooldownsSplitted[i]} sec
            </span>
        </div>
        </div>)}
    />
  </ListItem>);
}

export default ExerciseItem
