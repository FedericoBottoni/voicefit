import React, { useState, useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import Slider from '@material-ui/core/Slider';
import Input from '@material-ui/core/Input';
import FitnessCenter from '@material-ui/icons/FitnessCenter';
import Hotel from '@material-ui/icons/Hotel';

const MIN_REPS = 1;
const MAX_REPS = 50;

const MIN_CD = 10;
const MAX_CD = 120;


const useStyles = makeStyles({
  root: {
    display: 'flex',
    flexDirection: 'row',
    width: '100%',
  },
  input: {
    width: 42,
  },
  sliderContianer: {
    boxSizing: 'border-box',
    display: 'block',
    width: '50%',
    padding: '0 16px'
  }
});

/**
* This component renders the names and a little preview of a series of exercises
 * @param    serie Object containing the data about the series ie. the repetitions and the cooldowns
 * @param    setSerie Callback that allows to set the serie state
 */
export default function ExerciseRow({ serie, setSerie }) {
  const classes = useStyles();
  const { reps, cd } = serie || {};
  const [repsDisplayedValue, setRepsDisplayedValue] = useState(reps || 1);
  const [cdsDisplayedValue, setCdsDisplayedValue] = useState(cd || 1);

  useEffect(() => {
    setRepsDisplayedValue(reps);
    setCdsDisplayedValue(cd);
  }, [reps, cd])

  const repsSliderChange = (event, nextReps) => {
    setSerie({ cd, reps: nextReps });
  };
  const repsInputChange = (event) => {
    const nextReps = event.target.value === '' ? '' : Number(event.target.value);
    setSerie({ cd, reps: nextReps });
  };

  const cdsSliderChange = (event, nextCds) => {
    setSerie({ cd: nextCds, reps });
  };

  const cdsInputChange = (event) => {
    const nextCds = event.target.value === '' ? '' : Number(event.target.value);
    setSerie({ cd: nextCds, reps });
  };
  const cdsBlur = () => {
    if (cd < MIN_CD) {
      setSerie({ cd: MIN_CD, reps });
    } else if (cd > MAX_CD) {
      setSerie({ cd: MAX_CD, reps });
    }
  };

  const repsBlur = () => {
    if (reps < MIN_REPS) {
      setSerie({ cd, reps: MIN_REPS });
    } else if (reps > MAX_REPS) {
      setSerie({ cd, reps: MAX_REPS });
    }
  };

  return (
    <div className={classes.root}>
      <div className={classes.sliderContianer}>
        <Typography id="input-slider" gutterBottom>
          Ripetizioni
        </Typography>
        <Grid container spacing={2} alignItems="center">
          <Grid item>
          <FitnessCenter />
          </Grid>
          <Grid item xs>
            <Slider
              min={MIN_REPS}
              max={MAX_REPS}
              value={repsDisplayedValue}
              onChangeCommitted={repsSliderChange}
              onChange={(_, val) => setRepsDisplayedValue(val)}
              aria-labelledby="input-slider"
            />
          </Grid>
          <Grid item>
            <Input
              name="reps"
              className={classes.input}
              value={repsDisplayedValue}
              margin="dense"
              onChange={repsInputChange}
              onBlur={repsBlur}
              inputProps={{
                step: 10,
                min: 0,
                max: 100,
                type: 'number',
                'aria-labelledby': 'input-slider',
              }}
            />
          </Grid>
        </Grid>
      </div>
      <div className={classes.sliderContianer}>
        <Typography id="input-slider" gutterBottom>
          Riposo [s]
        </Typography>
        <Grid container spacing={2} alignItems="center">
          <Grid item>
            <Hotel />
          </Grid>
          <Grid item xs>
            <Slider
              min={MIN_CD}
              max={MAX_CD}
              value={cdsDisplayedValue}
              onChangeCommitted={cdsSliderChange}
              onChange={(_, val) => setCdsDisplayedValue(val)}
              aria-labelledby="input-slider"
            />
          </Grid>
          <Grid item>
            <Input
              name="cds"
              className={classes.input}
              value={cdsDisplayedValue}
              margin="dense"
              onChange={cdsInputChange}
              onBlur={cdsBlur}
              inputProps={{
                step: 10,
                min: 0,
                max: 100,
                type: 'number',
                'aria-labelledby': 'input-slider',
              }}
            />
          </Grid>
        </Grid>
      </div>
    </div>
  );
}
