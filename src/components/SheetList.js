import React, { useState, useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import InfoIcon from '@material-ui/icons/Info';
import IconButton from '@material-ui/core/IconButton';
import Paper from '@material-ui/core/Paper';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import CircularProgress from '@material-ui/core/CircularProgress';
import { getSheets } from '../api/sheet'
import SheetsStub from '../data/SheetsSchema.json'
import { useStub } from '../config';

const useStyles = makeStyles((theme) => ({
  list: {
    overflowY: 'auto',
    height: '100%'
  },
  paperRoot: {
    width: '100%',
    height: '100%'
  },
  duration: {
    paddingRight: 16
  },
  notes: {
    color: '#000'
  },
  wrapper: {
    margin: theme.spacing(1),
    position: 'relative',
    width: '100%',
    minWidth: 400
  },
  elementProgress: {
    position: 'absolute',
    top: '50%',
    left: '50%',
    marginTop: -12,
    marginLeft: -12,
  }
}));
/**
* This component renders the list of the avaiable list in a pre-defined set of dates
 * @param    selectedSheet Object containing the id of the selected sheet
 * @param    setSelectedSheet Callback used to set the selected sheet id
 * @param    setSelectedSheetInfo Callback used to set the selected sheet object
 * @param    setDefaultSnackbarError Callback which fires the snackbar error message
 */
const SheetList = ({ selectedSheet, setSelectedSheet, setSelectedSheetInfo, setDefaultSnackbarError }) => {
  const classes = useStyles();
  const [sheets, setSheets] = useState(null);
  const [sheetsLoading, setSheetsLoading] = useState(null);
  useEffect(() => {
    setSheetsLoading(true);
    (useStub ? Promise.resolve({data: SheetsStub}) : getSheets())
      .then(res => {
        setSheets(res.data);
        setSheetsLoading(false);
      })
      .catch(() => {
        setDefaultSnackbarError();
        setSheetsLoading(false);
      })
    }, [])

  const handleClick = (sheetId) => {
    setSelectedSheet(sheetId);
  };
  

  return (<div className={classes.wrapper}>
    <Paper classes={{
      root: classes.paperRoot
    }}>
      {sheets && <List className={classes.list}>
        {sheets.map((sheet) => (
            <ListItem button onClick={() => handleClick(sheet.id) } selected={sheet.id === selectedSheet}>
              <ListItemText
                primary={sheet.name}
                secondary={
                  <>
                    <span
                      className={classes.duration}
                    >
                      Durata {sheet.duration} min,
                    </span>
                    <span className={classes.notes}>
                      {sheet.exercises.length} esercizi{sheet.exercises.length === 1 ? 'o' : ''}
                    </span>
                  </>
                }
              />
              <ListItemSecondaryAction>
                <IconButton key={sheet.name} onClick={() => setSelectedSheetInfo(sheet)}>
                  <InfoIcon />
                </IconButton>
              </ListItemSecondaryAction>
            </ListItem>
          ))}
      </List>}
    </Paper>
    {sheetsLoading && <CircularProgress size={24} className={classes.elementProgress} />}
  </div>)
}

export default SheetList