import React, { useState, useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import TransferList from '../TransferList';
import exercisesStub from '../../data/ExerciseDetails.json'
import ExerciseDetails from '../ExerciseDetails.js';
import { getExercises } from '../../api/exercises';
import { useStub } from '../../config';

const useStyles = makeStyles((theme) => ({
  tlistContainer: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    padding: '16px 48px'
  },
  exercisesDetailsContainer: {
    padding: '16px 48px'
  },
  button: {
    display: 'flex',
    justifyContent: 'flex-end'
  },
  root: {
    display: 'flex',
    flexDirection: 'row',
    width: '100%',
    padding: 32
  },
  sheetNameContainer: {
    display: 'block',
    padding:'0 0 0 3%',
    alignItems: 'center',
    display: 'flex'
  },
  dialog: {
    minWidth: 1200
  }
}));
/**
* This component manages the creation of a new sheets
 * @param    setDefaultSnackbarError Callback which fires the snackbar error message
 * @param    createOpen Controls the role of the dialog: can be a TRUE if the dialog is open in created-mod, an object if it's in update-mod and FALSE if it's closed
 * @param    onDialogClose Callback which fires the dialog closure
 */

const CreateSheet = ({ setDefaultSnackbarError, createOpen, onDialogClose }) => {
  const classes = useStyles();
  
  const [exercises, setExercises] = useState(null);
  const [selectedExercises, setSelectedExercises] = useState([]);
  const [sheetName, setSheetName] = useState('Scheda di oggi');
  const [duration, setDuration] = useState(10);
/**
   * This state is used to store the exercises
   * */

  const exerciseListCallback = (_, right) => {
    const nextSelectedExercises = right.map(x => exercises.find(y => y.name === x));
    setSelectedExercises(nextSelectedExercises);
  }

  const handleSheetNameChange = (event) => {
    setSheetName(event.target.value === '' ? '' : event.target.value);
  };

  const handleDurationChange = (event) => {
    const parsed = parseInt(event.target.value, 10);
    if (isNaN(event.target.value) || isNaN(parsed)) {
      setDuration(0);
    } else {
      setDuration(parsed);
    }
  };

  const initLists = () => {
  (useStub ? Promise.resolve({data: exercisesStub}) : getExercises())
    .then(res => {
      setExercises(res.data);
    })
    .catch(() => {
      setDefaultSnackbarError();
    })
  }
  
  const createMod = typeof createOpen === 'boolean';
  
  useEffect(() => {
    setSheetName(sheetName);
    initLists();
  }, []);

  useEffect(() => {
    if(createOpen && !createMod) {
      setSheetName(createOpen.name);
      setDuration(createOpen.duration);
      setSelectedExercises(createOpen.exercises.sort((x,y) => x.order - y.order).map(x => x.exercise));
    }
    else if(!createOpen) {
      resetPageState();
    }
  }, [createOpen]);
  
  const resetPageState = () => {
    setSelectedExercises([]);
    setSheetName('Scheda di oggi');
    setExercises(null);
    initLists();
  }
  
  return (<Dialog open={!!createOpen} classes={{ paperWidthXl: classes.dialog }} maxWidth="xl">
    <div className={classes.root}>
      <div>
        <h1>{createMod ? 'Crea' : 'Modifica'} scheda</h1>
      </div>
      <div className={classes.sheetNameContainer}>
        <TextField required error={!sheetName} id="standard-basic" label="Nome scheda" value={sheetName} onChange={handleSheetNameChange} style={{ paddingRight: 24 }} />
        <TextField required error={!duration || duration.length <= 0}  label="Durata stimata (min)" value={duration} onChange={handleDurationChange}/>
      </div>
    </div>
    {exercises &&
      <>
        <div className={classes.tlistContainer}>
          <TransferList
            title="Esercizi"
            initLeft={exercises.filter(ex => selectedExercises.map(x => x.id).indexOf(ex.id) === -1).map(ex => ex.name || ex.schema)}
            initRight={selectedExercises.map(ex => ex.name || ex.schema)}
            getStateCallback={exerciseListCallback} />
        </div>
        <div className={classes.exercisesDetailsContainer}>
          <ExerciseDetails
            setDefaultSnackbarError={setDefaultSnackbarError}
            resetPageState={resetPageState} sheetName={sheetName}
            exercises={selectedExercises}
            setExercises={setSelectedExercises}
            onClose={onDialogClose}
            duration={duration}
            createMod={createMod}
            sheetId={createOpen && createOpen.id}
          />
        </div>
      </>
    }
  </Dialog>);
}

export default CreateSheet;
