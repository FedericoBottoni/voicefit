
import React from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import CircularProgress from '@material-ui/core/CircularProgress';
import List from '@material-ui/core/List';
import DialogTitle from '@material-ui/core/DialogTitle';
import { makeStyles } from '@material-ui/core/styles';
import ExerciseItem from '../ExerciseItem';

const useStyles = makeStyles((theme) => ({
  list: {
    maxWidth: 350
  },
  listContainer: {
    display: 'flex',
  },
  wrapper: {
    margin: theme.spacing(1),
    position: 'relative',
  },
  buttonProgress: {
    position: 'absolute',
    top: '50%',
    left: '50%',
    marginTop: -12,
    marginLeft: -12,
  },
}));

/**
  * The component is a dialog which require the confirmation of the sheet deletion from the user
  * @param    sheetToDelete Object that controls the dialog: if it exists the dialog is open and it is filled by its attributes, otherwise it's closed
  * @param    onDeleteConfirm Callback fired on the confirm button click, it concerns the api call and the deletion
  * @param    onClose Callback which fires the dialog closure
  */
const DeleteConfirmDialog = ({ sheetToDelete, onDeleteConfirm, loading, setLoading, onClose }) => {
  const { name, duration, exercises } = sheetToDelete || {};
  const classes = useStyles();
  return (
    <Dialog
      onClose={onClose}
      open={!!sheetToDelete}
    >
      <DialogTitle>Conferma l'elimazione della scheda "{name}"</DialogTitle>
      <DialogContent>
        Durata {duration} min.<br />
        Lista esercizi: <br />
        <div className={classes.listContainer}>
          <List classes={{root: classes.list}}>
            {exercises && exercises.sort((x,y) => x.order - y.order).map((ex, index) => {
              const { schema, repetitions, cooldown } = ex.exercise;
              const repetitionsSplitted = repetitions.split(' ');
              const cooldownsSplitted = cooldown.split(' ');
              return <ExerciseItem index={index} schema={schema} repetitionsSplitted={repetitionsSplitted} cooldownsSplitted={cooldownsSplitted} />})}
          </List>
        </div>
      </DialogContent>
      <DialogActions>
        <Button onClick={onClose} color="secondary" autoFocus>
          Chiudi
        </Button>
        <div className={classes.wrapper}>
          <Button disabled={loading} onClick={onDeleteConfirm} color="primary">
            Conferma
          </Button>
          {loading && <CircularProgress size={24} className={classes.buttonProgress} />}
        </div>
      </DialogActions>
    </Dialog>
  );
}

export default DeleteConfirmDialog;