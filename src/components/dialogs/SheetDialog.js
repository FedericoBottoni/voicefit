
import React from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import List from '@material-ui/core/List';
import Paper from '@material-ui/core/Paper';
import { makeStyles } from '@material-ui/core/styles';
import ExerciseItem from '../ExerciseItem';

const useStyles = makeStyles((theme) => ({
  list: {
    maxWidth: 300
  },
  content:{
    color: '#000'
  },
  paperRoot: {
    width: '100%',
    minWidth: 300
  },
  reps: {
    padding: '4px 16px',
    display: 'flex'
  },
  cooldowns: {
    padding: '4px 16px',
    display: 'flex'
  },
  listItemText: {
    display: 'flex',
    flexDirection: 'column'
  },
  
  listItemTextRow: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  icon: {
    marginRight: 6
  }
  }));

/**
  * The component is a dialog which require the confirmation of the sheet deletion from the user
  * @param    sheet Object that controls the dialog: if it exists the dialog is open and it is filled by its attributes, otherwise it's closed
  * @param    onClose Callback which fires the dialog closure
  */
const SheetDialog = ({ sheet, onClose }) => {
  const { name, duration, exercises } = sheet || {};
  const classes = useStyles();
  return (
  <Paper classes={{
        root: classes.paperRoot
      }}>
    <Dialog
      onClose={onClose}
      open={!!sheet}
    >
      <DialogTitle>Scheda: {name}</DialogTitle>
      <DialogContent>
        <DialogContentText className={classes.content}>
          Durata {duration} min.<br />
          Lista esercizi: <br />
          <List classes={{ root: classes.list }}>
            {exercises && exercises.sort((x,y) => x.order - y.order).map((ex, index) => {
              const { schema, repetitions, cooldown } = ex.exercise;
              const repetitionsSplitted = repetitions.split(' ');
              const cooldownsSplitted = cooldown.split(' ');
              return <ExerciseItem index={index} schema={schema} repetitionsSplitted={repetitionsSplitted} cooldownsSplitted={cooldownsSplitted} />})}
          </List>
          
        </DialogContentText>
        
      </DialogContent>
      <DialogActions>
        <Button onClick={onClose} color="primary" autoFocus>
          Chiudi
        </Button>
      </DialogActions>
    </Dialog>
  </Paper>
  );
}

export default SheetDialog;