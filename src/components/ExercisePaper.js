import React, { useRef } from 'react';
import { useDrag, useDrop } from 'react-dnd';
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import Avatar from '@material-ui/core/Avatar';
import IconButton from '@material-ui/core/IconButton';
import DeleteTwoToneIcon from '@material-ui/icons/DeleteTwoTone';
import AddIcon from '@material-ui/icons/Add';
import ReorderIcon from '@material-ui/icons/Reorder';
import ExerciseRow from '../components/ExerciseRow.js';

const useStyles = makeStyles((theme) => ({
  paperContainer: {
      width: 'calc(50% - 16px)',
      boxSizing: 'border-box',
      width: '50%'
    },
    paper: {
      margin: theme.spacing(1),
      padding: theme.spacing(2),
    },
    exerciseNameContainer: {
      display: 'flex',
      alignItems: 'center'
    },
    exerciseName: {
      padding: 0,
      margin: 0
    },
    container: {
      padding: 8
    },
    button: {
      display: 'flex',
      justifyContent: 'flex-end',
      paddingTop: 8
    },
    row: {
      display: 'flex',
      justifyContent: 'space-between',
      width: '100%'
    },
    dragContainer: {
      display: 'flex',
      justifyContent: 'flex-end'
    },
    dragButton: {
      padding: 8,
      cursor: 'all-scroll'
    }
  }));
/**
* This component gives the details of a new sheets
*  @param    series Object containing all the info about the series
 * @param    moveCard Event that triggers when a card is moved
 * @param    setSerie Callback used to edit the serie state of a single element 
 * @param    setSeries Callback used to edit the serie state of multiple element 
 * @param    deleteSerie Callback used to manage the remotion of a serie
 * @param    ItemTypes Object containing the Exercise item type
 * @param    exercise Object containing all the exercises
 * @param    index Object containing the index of the dragged item
 * @param    dragItem Object containing the dragged item
 * @param    setDragItem Callback used to set the dragged item state
 * @param    onDrop Event that manages the drop 
 */
const ExercisePaper = ({
  series,
  moveCard,
  setSerie,
  setSeries,
  addSerie,
  deleteSerie,
  ItemTypes,
  exercise,
  index,
  dragItem,
  setDragItem,
  onDrop }) => {
    const classes = useStyles();
    const currentSeries = series.filter(x => x.exercise === exercise.id);
    const ref = useRef(null);

    const [{ isDragging }, drag] = useDrag({
      item: {type: ItemTypes.EXERCISE, id: exercise.id, index },
      begin: (monitor) => {
        setDragItem(exercise)
      },
      end: (item, monitor) => {
        setDragItem(null)
      },
      collect: (monitor) => ({
        isDragging: monitor.isDragging(),
      }),
    })
    const [, drop] = useDrop({
      accept: ItemTypes.EXERCISE,
      hover(item, monitor) {
        if (!ref.current) {
          return
        }
        const dragIndex = item.index;
        const hoverIndex = index;
        // Don't replace items with themselves
        if (dragIndex === hoverIndex) {
          return;
        }
        // Determine rectangle on screen
        const hoverBoundingRect = ref.current.getBoundingClientRect()
        // Get vertical middle
        const hoverMiddleY =
          (hoverBoundingRect.bottom - hoverBoundingRect.top) / 2
        // Determine mouse position
        const clientOffset = monitor.getClientOffset()
        // Get pixels to the top
        const hoverClientY = clientOffset.y - hoverBoundingRect.top
        // Only perform the move when the mouse has crossed half of the items height
        // When dragging downwards, only move when the cursor is below 50%
        // When dragging upwards, only move when the cursor is above 50%
        // Dragging downwards
        if (dragIndex < hoverIndex && hoverClientY < hoverMiddleY) {
          return
        }
        // Dragging upwards
        if (dragIndex > hoverIndex && hoverClientY > hoverMiddleY) {
          return
        }
        // Time to actually perform the action
        moveCard(dragIndex, hoverIndex)
        // Note: we're mutating the monitor item here!
        // Generally it's better to avoid mutations,
        // but it's good here for the sake of performance
        // to avoid expensive index searches.
        item.index = hoverIndex
      },
      drop: (item, monitor) => onDrop(item)
    })
    drop(ref);

    return (
      <div className={classes.paperContainer} ref={ref}>
        <Paper className={classes.paper} style={{ visibility: exercise.id ? 'visible' : 'hidden', opacity: dragItem && dragItem.id === exercise.id ? 0 : 1 }} >
        <Grid container wrap="nowrap" spacing={2}>
            <Grid item>
            <Avatar>{index+1}</Avatar>
            </Grid>
            <Grid item xs className={classes.exerciseNameContainer}>
            <h3 className={classes.exerciseName}>{exercise.name}</h3>
            </Grid>
            <Grid item xs className={classes.dragContainer}>
            <div className={classes.dragButton} ref={drag}><ReorderIcon /></div>
            </Grid>
        </Grid>
        <div className={classes.container}>
            {currentSeries.sort((x, y) => x.order - y.order).map(serie =>
            <div className={classes.row}>
                <ExerciseRow serie={serie} setSerie={(serieObject) => setSerie({ ...serie, ...serieObject })} />
                
                <IconButton disabled={currentSeries.length <= 1} onClick={() => deleteSerie(serie, series, setSeries)}>
                  <DeleteTwoToneIcon />
                </IconButton>
            </div>)}
            <div className={classes.row}>
                <IconButton onClick={() => addSerie(exercise, series, setSeries)}>
                  <AddIcon />
                </IconButton>
            </div>
        </div>
        </Paper>
      </div> )}

export default ExercisePaper;