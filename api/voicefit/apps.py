from django.apps import AppConfig


class VoicefitConfig(AppConfig):
    name = 'voicefit'
