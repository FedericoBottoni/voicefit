def is_female(gender):
  return gender != None and gender.upper() == 'F'

def is_male(gender):
  return gender != None and gender.upper() == 'M'
  
def is_other(gender):
  return gender != None and gender.upper() == 'O'