import requests
import json

def setUser(attrs):
    r = requests.post(url = "http://127.0.0.1:53871/setUser", json = attrs, timeout=10)
    return(json.loads(r.text))

def getUser(attrs):
    r = requests.post(url = "http://127.0.0.1:53871/getUser", json = attrs, timeout=10)
    return(json.loads(r.text))

def setAttr(attrs):
    r = requests.post(url = "http://127.0.0.1:53871/setAttribute", json = attrs, timeout=10)
    return(json.loads(r.text))

def getAttr(attrs):
    r = requests.post(url = "http://127.0.0.1:53871/getAttribute", json = attrs, timeout=10)
    return(json.loads(r.text))