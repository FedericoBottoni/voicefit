# todos/serializers.py
from rest_framework import serializers
from .models import *


class CustomerSerializer(serializers.ModelSerializer):
    class Meta:
        model = Customer
        fields = (
            'id',
            'name',
            'surname',
            'dob',
            'codFis',
            'gender',
        )

class ExerciseSchemaSerializer(serializers.ModelSerializer):
    class Meta:
        model = ExerciseSchema        
        fields = (
            'id',
            'name',
            'description',
        )


class ExerciseSerializer(serializers.ModelSerializer):
    schema = serializers.CharField(source='exerciseSchemaId.name', read_only='true')
    schema_description = serializers.CharField(source='exerciseSchemaId.description', read_only='true')
    schema_id = serializers.IntegerField(source='exerciseSchemaId.id', read_only='true')
    
    class Meta:
        model = Exercise
        fields = (
            'id',
            'repetitions',
            'cooldown',
            'notes',
            'schema',
            'schema_description',
            'schema_id',
        )

class SheetExerciseSerializer(serializers.ModelSerializer):
    exercise = ExerciseSerializer()
    
    class Meta:
        model = SheetExercise
        fields = (
            'exercise',
            'order',
        )
        

class SheetSerializer(serializers.ModelSerializer):
    exercises = SheetExerciseSerializer(source='sheetexercise_set', many='true', read_only=True)

    class Meta:
        model = Sheet
        fields = (
            'id',
            'name',
            'duration',
            'exercises',
        )
        depth = 1