from django.db import models
from django.contrib import admin

GENDER = (
        ('M', 'Male'),
        ('F', 'Female'),
        ('O', 'Other'),
 )

class Trainer(models.Model):
    name = models.CharField(max_length=50)
    surname = models.CharField(max_length=50)
    dob = models.DateField()
    codFis = models.CharField(max_length=16)
    gender = models.CharField(
        max_length=1,
        choices=GENDER,
        default='M'
    )
    address = models.CharField(max_length=100)
    username = models.CharField(max_length=30)
    pwd = models.CharField(max_length=128)
    active = models.BooleanField(default=True)
    tester = models.BooleanField(default=False)


class ExerciseSchema(models.Model):
    name = models.CharField(max_length=30)
    active = models.BooleanField(default=True)
    description = models.CharField(max_length=500)


class Customer(models.Model):
    name = models.CharField(max_length=50)
    surname = models.CharField(max_length=50)
    dob = models.DateField()
    codFis = models.CharField(max_length=16)
    gender = models.CharField(
        max_length=1,
        choices=GENDER,
        default='M'
    )
    address = models.CharField(max_length=100)
    username = models.CharField(max_length=30)
    pwd = models.CharField(max_length=128)
    active = models.BooleanField(default=True)
    tester = models.BooleanField(default=False)
    trainerId = models.ForeignKey(Trainer, on_delete=models.CASCADE, blank=True, null=True)

class Exercise(models.Model):
    repetitions = models.CharField(max_length=30)
    cooldown = models.CharField(max_length=30)
    notes = models.TextField()
    trainerId = models.ForeignKey(Trainer, on_delete=models.CASCADE)
    exerciseSchemaId = models.ForeignKey(ExerciseSchema, on_delete=models.CASCADE)
    customer = models.ManyToManyField(Customer, through='ExerciseCustomer', blank=True)
    active = models.BooleanField(default=True)

class Sheet(models.Model):
    name = models.CharField(max_length=30)
    duration = models.IntegerField()
    trainerId = models.ForeignKey(Trainer, on_delete=models.CASCADE)
    exercise = models.ManyToManyField(Exercise, through='SheetExercise', blank=True)
    customer = models.ManyToManyField(Customer, through='SheetCustomer', blank=True)
    active = models.BooleanField(default=True)

class SheetCustomer(models.Model):
    sheetCustomer_id = models.AutoField(primary_key=True)
    sheet = models.ForeignKey(Sheet, on_delete=models.CASCADE)
    customer = models.ForeignKey(Customer, on_delete=models.CASCADE)
    date = models.DateField()
    start = models.DateTimeField(null=True)
    end = models.DateTimeField(null=True)
    
    class Meta:
        unique_together = (("sheet", "customer", "date"),)

class SheetCustomerInline(admin.TabularInline):
    model = SheetCustomer
    extra = 1

class SheetExercise(models.Model):
    sheet = models.ForeignKey(Sheet, on_delete=models.CASCADE)
    exercise = models.ForeignKey(Exercise, on_delete=models.CASCADE)
    order = models.IntegerField()

class SheetExerciseInline(admin.TabularInline):
    model = SheetExercise
    extra = 1


class ExerciseCustomer(models.Model):
    exerciseCustomer_id = models.AutoField(primary_key=True)
    exercise = models.ForeignKey(Exercise, on_delete=models.CASCADE)
    customer = models.ForeignKey(Customer, on_delete=models.CASCADE)
    date = models.DateField()
    start = models.DateTimeField(null=True)
    end = models.DateTimeField(null=True)
    
    class Meta:
        unique_together = (("exercise", "customer", "date"),)

class ExerciseCustomerInline(admin.TabularInline):
    model = ExerciseCustomer
    extra = 1

class Admin(models.Model):
    username = models.CharField(max_length=30)
    pwd = models.CharField(max_length=128)


class ExerciseAdmin(admin.ModelAdmin):
    inlines = (ExerciseCustomerInline, SheetExerciseInline,)

class SheetAdmin(admin.ModelAdmin):
    inlines = (SheetCustomerInline, SheetExerciseInline,)

class CustomerAdmin(admin.ModelAdmin):
    inlines = (ExerciseCustomerInline, SheetCustomerInline,)