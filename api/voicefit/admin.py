from django.contrib import admin

from .models import *

admin.site.register(Customer, CustomerAdmin)
admin.site.register(Trainer)
admin.site.register(Sheet, SheetAdmin)
admin.site.register(Admin)
admin.site.register(Exercise, ExerciseAdmin)
admin.site.register(ExerciseSchema)

