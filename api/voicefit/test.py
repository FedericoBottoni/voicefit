import json

from django.contrib.auth.models import User
from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase


class TestCalls(APITestCase):
    fixtures = ['test_data.json']
    def test_customer_list(self):
        response = self.client.get(reverse('voicefit:customer_list'))
        self.assertTrue(response)

    def test_sheet_list(self):
        response = self.client.get(reverse('voicefit:sheet_list'))
        self.assertTrue(response)

    def test_exercise_schema_list(self):
        response = self.client.get(reverse('voicefit:exercise_schema_list'))
        self.assertTrue(response)
        
    def test_available_customers(self):
        response = self.client.post(reverse('voicefit:getavailable'), {'dates': ["2020-06-04T14:55:07.720Z", "2020-06-05T14:55:07.720Z", "2020-06-06T14:55:07.720Z"]})
        self.assertTrue(response)
        response = self.client.post(reverse('voicefit:getavailable'), {'dates': []})
        self.assertEqual(response.status_code, status.HTTP_500_INTERNAL_SERVER_ERROR)
        