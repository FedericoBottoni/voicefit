from django.urls import path
from django_ask_sdk.skill_adapter import SkillAdapter
from . import views
from .skill_api.index import skill

voicefit_skill_view = SkillAdapter.as_view(
    skill=skill,
    verify_signature=True,
    verify_timestamp=True)

app_name = 'voicefit'
urlpatterns = [
    path('customer/getall', views.customer_list, name='customer_list'),
    path('exercise/getall', views.exercise_schema_list, name='exercise_schema_list'),
    path('sheet/getall', views.sheet_list, name='sheet_list'),
    path('sheet/assign', views.assign_sheet),
    path('customer/getavailable', views.available_customers, name='getavailable'),
    path('sheet/create', views.create_sheet),
    path('sheet/delete/<int:pk>', views.delete_sheet),
    path('sheet/update', views.update_sheet),
    #path('<int:pk>/', views.Detail.as_view()),
    path('alexa/', voicefit_skill_view)
]