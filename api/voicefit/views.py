from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response
from .serializers import *
from .models import *
import json
from datetime import date, datetime
from rest_framework.decorators import parser_classes
from rest_framework.parsers import JSONParser
from django.db import transaction


# Returns the customer list of the logged trainer
@api_view(['GET'])
def customer_list(request):
    if request.method == 'GET':
        trainer = 1
        queryset = Customer.objects.filter(trainerId=trainer)
        serializer = CustomerSerializer(queryset, many=True)
        return Response(serializer.data)

# Returns the exercise schema list
@api_view(['GET'])
def exercise_schema_list(request):
    if request.method == 'GET':
        queryset = ExerciseSchema.objects.all()
        serializer = ExerciseSchemaSerializer(queryset, many=True)
        return Response(serializer.data)

# Returns the sheets created by the logged trainer
def return_sheets(trainer):
    sheets = Sheet.objects.filter(trainerId=trainer, active=True)
    serializer = SheetSerializer(sheets, many=True)
    return serializer

@api_view(['GET'])
def sheet_list(request):
    if request.method == 'GET':
        trainer = 1
        serializer = return_sheets(trainer)
        return Response(serializer.data)

# Returns the list of availble customer in the specified dates
@api_view(['POST'])
@parser_classes([JSONParser])
def available_customers(request):
    if request.method == 'POST':
        trainer = 1

        data = request.data
        dates = list(data['dates'])

        if dates:
            date_list = []
            for d in dates:
                date = d[0:10]
                date = datetime.strptime(date, "%Y-%m-%d").date() 
                date_list.append(date)
            
            all_customer = Customer.objects.filter(trainerId=trainer)
            all_customer_ids = []
            for c in all_customer:
                all_customer_ids.append(c.id)
                
            sheet_customer = SheetCustomer.objects.all()

            not_available = []
            for s in sheet_customer:
                if s.date in date_list:
                    not_available.append(s.customer_id)

            available = list(set(all_customer_ids) - set(not_available))
            available = Customer.objects.filter(pk__in=available)

            serializer = CustomerSerializer(available, many=True)
            return Response(serializer.data)
        else:
            return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR)



def create(trainer_id, sheet_name, duration, order, exercise_schema_ids, repetitions, cooldowns, notes):
    exercises = []
    sheets = []

    for i in range(len(exercise_schema_ids)):
        e = Exercise.objects.create(
            repetitions = repetitions[i],
            cooldown = cooldowns[i],
            notes = notes[i],
            trainerId_id = trainer_id,
            exerciseSchemaId_id = exercise_schema_ids[i]
        )
        exercises.append(e)

    s = Sheet.objects.create(
        name = sheet_name,
        duration = duration,
        trainerId_id = trainer_id
    )
    
    for i in range(len(exercises)):
        se = SheetExercise.objects.create(
            sheet_id = s.id,
            exercise_id = exercises[i].id,
            order = order[i]
        )
    
    result_status = status.HTTP_201_CREATED

    return s.id, result_status, exercises

# Adds a new sheet record in the Sheet table
@api_view(['POST'])
@parser_classes([JSONParser])
@transaction.atomic
def create_sheet(request):
    if request.method == 'POST':
        trainer_id = 1
        data = request.data
        sheet_name = data['sheet_name']
        duration = int(data['duration'])
        order = list(data['order'])
        exercise_schema_ids = list(data['exercise_schema_id'])
        repetitions = list(data['repetitions'])
        cooldowns = list(data['cooldowns'])
        notes = list(data['notes'])
        
        _, status, _ = create(trainer_id, sheet_name, duration, order, exercise_schema_ids, repetitions, cooldowns, notes)
        
        return Response(status=status)


# Assigns a sheet to the customers specified by the logged trainer
@api_view(['POST'])
@parser_classes([JSONParser])
@transaction.atomic
def assign_sheet(request):
    if request.method == 'POST':
        data = request.data

        customer_ids = list(data['customerIds'])
        sheet = int(data['sheetId'])
        date_list = list(data['dates'])

        customer_ids = [int(i) for i in customer_ids] 

        processed_dates = []
        for d in date_list:
            d = d[0:10]
            date = datetime.strptime(d, "%Y-%m-%d").date()
            processed_dates.append(date)

        for c in customer_ids:
            for d in processed_dates:
                SheetCustomer.objects.create(
                    customer_id = c,
                    sheet_id = sheet,
                    date = d,
                )

        se_list = SheetExercise.objects.filter(sheet_id=sheet)

        e_ids = []
        for s in se_list:
            e_ids.append(s.exercise_id)
        
        for c in customer_ids:
            for d in processed_dates:
                for e in e_ids:
                    ExerciseCustomer.objects.create(
                        exercise_id = e,
                        customer_id = c,
                        date = d,
                    )

        return Response(status=status.HTTP_202_ACCEPTED)

#Delete selected sheet
@api_view(['DELETE'])
@transaction.atomic
def delete_sheet(request, pk):
    if request.method == 'DELETE':
        trainer = 1
        sheet = Sheet.objects.get(pk=pk)
        sheet.active = False
        sheet.save()

        sc_list = SheetCustomer.objects.filter(sheet=pk, start=None)
        customers = []
        for sc in sc_list:
            customers.append((sc.customer, sc.date))

        sc_list.delete()

        se_list = SheetExercise.objects.filter(sheet=pk)
        exercises = [x.exercise.id for x in se_list]
        
        for e in exercises:
            ec_list = ExerciseCustomer.objects.filter(exercise=e)
            for ec in ec_list:
                if (ec.customer, ec.date) in customers:
                    ec.delete()
        
        serializer = return_sheets(trainer)
        return Response(serializer.data)

@api_view(['PUT'])
@transaction.atomic
def update_sheet(request):
    if request.method == 'PUT':
        trainer = 1
        data = request.data
        old_sheet_id = int(data['sheetId'])
        name = data['sheet_name']
        duration = int(data['duration'])
        order = list(data['order'])
        exercise_schema_ids = list(data['exercise_schema_id'])
        repetitions = list(data['repetitions'])
        cooldowns = list(data['cooldowns'])
        notes = list(data['notes'])

        #"Spegnimento" della scheda vecchia
        old_sheet = Sheet.objects.get(pk=old_sheet_id)
        old_sheet.active = False
        old_sheet.save()

        #Creazione della nuova scheda
        new_sheet_id, _,  new_exercises = create(trainer, name, duration, order, exercise_schema_ids, repetitions, cooldowns, notes)

        #Update dei riferimenti dalla vecchia alla nuova scheda - SheetCustomer
        sc_list = SheetCustomer.objects.filter(sheet=old_sheet_id, start=None)
        new_sheet = Sheet.objects.get(pk=new_sheet_id)
        customers = []
        for sc in sc_list:
            sc.sheet = new_sheet
            sc.save()
            customers.append((sc.customer, sc.date))

        #Update dei riferimenti dalla vecchia alla nuova scheda - ExerciseCustomer
        se_list = SheetExercise.objects.filter(sheet=old_sheet_id)
        exercises = [x.exercise.id for x in se_list]
        
        for e in exercises:
            ec_list = ExerciseCustomer.objects.filter(exercise=e)
            for ec in ec_list:
                if (ec.customer, ec.date) in customers:
                    ec.delete()
        
        for e in new_exercises:
            for c in customers:
                ExerciseCustomer.objects.create(
                            exercise_id = e.id,
                            customer_id = c[0].id,
                            date = c[1],
                        )
        return Response(status=status.HTTP_202_ACCEPTED)

