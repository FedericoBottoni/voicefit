from .models import *
from django.db import transaction
from datetime import datetime
import pytz
from tzlocal import get_localzone
from .utils.persistence_manager import getAttr, setAttr, getUser, setUser

## IMPORTANTE --> TO DO:
## - Usare id dell'utente loggato una volta implementato il login
## - Usare data odierna e non data fittizia

FULL_DATE = '%Y-%m-%d'
FULL_TIME = '%H:%M:%S'
FULL_DATE_TIME = '%Y-%m-%d %H:%M:%S'

def getCustomer(customer_key):
    customer = Customer.objects.get(pk = customer_key)
    return customer

def refreshSheetCache(customer_key, customer_gender, customer_name, completed, sheet_id, sheet_name, duration, exercises, exercises_names):
    r = setAttr({"user": str(customer_key), "attributes":
                         {"gender": customer_gender,
                          "name": customer_name,
                          "completed": str(completed),
                          "started": "False",
                          "sheet_name": sheet_name,
                          "sheet_id": str(sheet_id),
                          "duration": str(duration),
                          "exercises_names": exercises_names,
                          "exercises": {i:{"repetitions":exercises[i].repetitions, "cooldown":exercises[i].cooldown, "notes":exercises[i].notes} for i in exercises.keys()}
                         }
                        })
    return None

def setCustomer(customer_id, customer_session_key, date):
    customer = getCustomer(customer_id)
    if customer is not None:
        r = setUser({"user": str(customer_id), "attributes":{"session": customer_session_key, "date": date}})
        if r != {} and r["date"] == date:
            r = getAttr({"user": str(customer_id), "attributes":["started"]})
            if r["started"] == "True":
                setAttr({"user": str(customer_id), "attributes":{"reprise": "1"}})
            else:
                setAttr({"user": str(customer_id), "attributes":{"reprise": "0"}})
        else:
            setAttr({"user": str(customer_id), "attributes":{"reprise": "0"}})
        completed, sheet_id, sheet_name, duration, exercises, exercises_names = getTrainingSheet(customer_session_key)
        if sheet_id is not None:
            refreshSheetCache(customer_id, customer.gender, customer.name, completed, sheet_id, sheet_name, duration, exercises, exercises_names)
        else:
            r = setAttr({"user": str(customer_id), "attributes":{"gender": customer.gender, "name": customer.name, "completed": str(completed), "started": "False"}})
        res = True
    else:
        res = False
    return res

def getCustomerInfo(customer_session_key):
    u = getUser({"session": customer_session_key})
    r = getAttr({"user": u["user"], "attributes":["date", "gender", "name", "completed", "started"]})
    return r

def getTrainingSheetInfo(customer_session_key):
    u = getUser({"session": customer_session_key})
    r = getAttr({"user": u["user"], "attributes":["date", "sheet_name", "sheet_id", "duration", "exercises_names", "exercises"]})
    return r

def getTodaySheetCustomer(customer_key):
    r = getAttr({"user": customer_key, "attributes": ["date"]})
    print(r)
    date_now = pytz.utc.localize(datetime.strptime(r["date"], FULL_DATE)).date()
    try:
        today_sheet = SheetCustomer.objects.get(customer_id=int(customer_key), date=date_now)
    except SheetCustomer.DoesNotExist:
        today_sheet = None
    return today_sheet

def getTrainingSheet(customer_session_key):
    u = getUser({"session": customer_session_key})
    today_sheet = getTodaySheetCustomer(u["user"])
    if today_sheet is not None:
        completed = True if today_sheet.end is not None else False
        sheet = Sheet.objects.get(pk = today_sheet.sheet_id)
        sheet_id = sheet.id
        sheet_name = sheet.name
        duration = sheet.duration
        sheetExercises = SheetExercise.objects.all().filter(sheet_id = sheet.id)
        exercises_names = dict()
        exercises = dict()
        for e in sheetExercises:
            order = str(e.order)
            exercise = Exercise.objects.get(pk = e.exercise_id)
            exercises[order] = exercise
            exercise_schema = ExerciseSchema.objects.get(pk = exercise.exerciseSchemaId_id)
            exercises_names[order] = exercise_schema.name
    else:
        completed = sheet_id = sheet_name = duration = exercises = exercises_names = None
    
    return completed, sheet_id, sheet_name, duration, exercises, exercises_names

@transaction.atomic
def startSheet(customer_session_key, date_time):
    u = getUser({"session": customer_session_key})
    customer = getCustomer(int(u["user"]))
    completed, sheet_id, sheet_name, duration, exercises, exercises_names = getTrainingSheet(customer_session_key)
    if sheet_id is not None:
        refreshSheetCache(customer.id, customer.gender, customer.name, completed, sheet_id, sheet_name, duration, exercises, exercises_names)
        r = getAttr({"user": u["user"], "attributes":["reprise"]})
        print(r)
        if r["reprise"] == "0":
            today_sheet = getTodaySheetCustomer(u["user"])
            today_sheet.start = pytz.utc.localize(datetime.strptime(date_time, FULL_DATE_TIME))
            today_sheet.save()
            setAttr({"user": u["user"], "attributes":{"order_id":"1", "reps_id":"1"}})
            st = startExercise(customer_session_key, date_time)
        setAttr({"user": u["user"], "attributes":{"started":"True"}})
        res = getAttr({"user": u["user"], "attributes":["order_id", "reps_id"]})
    else:
        res = {}
    return res

def startExercise(customer_session_key, date_time):
    u = getUser({"session": customer_session_key})
    r = getAttr({"user": u["user"], "attributes":["order_id", "date", "sheet_id"]})
    try:
        sheetExercise = SheetExercise.objects.get(sheet_id = int(r["sheet_id"]), order = int(r["order_id"]))
        exerciseCustomer = ExerciseCustomer.objects.get(exercise_id = sheetExercise.exercise_id, customer_id = int(u["user"]), date = pytz.utc.localize(datetime.strptime(r["date"], FULL_DATE)).date())
        exerciseCustomer.start = pytz.utc.localize(datetime.strptime(date_time, FULL_DATE_TIME))
        exerciseCustomer.save()
        res = True
    except SheetExercise.DoesNotExist:
        res = False
    return res

@transaction.atomic
def skipExercise(customer_session_key, date_time):
    u = getUser({"session": customer_session_key})
    r = getAttr({"user": u["user"], "attributes":["order_id", "date", "sheet_id"]})
    sheetExercise = SheetExercise.objects.get(sheet_id = int(r["sheet_id"]), order = int(r["order_id"]))
    exerciseCustomer = ExerciseCustomer.objects.get(exercise_id = sheetExercise.exercise_id, customer_id = int(u["user"]), date = pytz.utc.localize(datetime.strptime(r["date"], FULL_DATE)).date())
    exerciseCustomer.end = exerciseCustomer.start
    exerciseCustomer.save()
    r["order_id"] = str(int(r["order_id"])+1)
    r["reps_id"] = "1"
    setAttr({"user": u["user"], "attributes":{"order_id":r["order_id"], "reps_id":r["reps_id"]}})
    res = startExercise(customer_session_key, date_time)
    if(not(res)):
        endSheet(customer_session_key, date_time)
        r = {}
    return r

@transaction.atomic
def endReps(customer_session_key, date_time):
    u = getUser({"session": customer_session_key})
    r = getAttr({"user": u["user"], "attributes":["order_id", "reps_id", "exercises", "sheet_id", "date"]})
    if int(r["reps_id"])+1 <= len(r["exercises"][r["order_id"]]["repetitions"].split(' ')):
        r["reps_id"] = str(int(r["reps_id"])+1)
        setAttr({"user": u["user"], "attributes":{"reps_id":r["reps_id"]}})
    else:
        sheetExercise = SheetExercise.objects.get(sheet_id = int(r["sheet_id"]), order = int(r["order_id"]))
        exerciseCustomer = ExerciseCustomer.objects.get(exercise_id = sheetExercise.exercise_id, customer_id = int(u["user"]), date = pytz.utc.localize(datetime.strptime(r["date"], FULL_DATE)).date())
        exerciseCustomer.end = pytz.utc.localize(datetime.strptime(date_time, FULL_DATE_TIME))
        exerciseCustomer.save()
        r["order_id"] = str(int(r["order_id"])+1)
        r["reps_id"] = "1"
        setAttr({"user": u["user"], "attributes":{"order_id":r["order_id"], "reps_id":r["reps_id"]}})
        res = startExercise(customer_session_key, date_time)
        if(not(res)):
            endSheet(customer_session_key, date_time)
            r = {}
    return r

def endSheet(customer_session_key, date_time):
    u = getUser({"session": customer_session_key})
    today_sheet = getTodaySheetCustomer(u["user"])
    today_sheet.end = pytz.utc.localize(datetime.strptime(date_time, FULL_DATE_TIME))
    today_sheet.save()
    setAttr({"user": u["user"], "attributes":{"completed":"True"}})
    return None