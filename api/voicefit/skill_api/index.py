# -*- coding: utf-8 -*-

#from ask_sdk_core.skill_builder import SkillBuilder
from ask_sdk_core.skill_builder import CustomSkillBuilder
from ask_sdk_core.api_client import DefaultApiClient
from .utils_handlers import HelpIntentHandler, CancelOrStopIntentHandler, SessionEndedRequestHandler, IntentReflectorHandler
from .home_handlers import LaunchRequestHandler, DailyTrainingHandler, StartSheetHandler, SkipExerciseHandler, EndRepsHandler
from .errors_handlers import CatchAllExceptionHandler

sb = CustomSkillBuilder(api_client=DefaultApiClient())
sb.add_request_handler(LaunchRequestHandler())
sb.add_request_handler(DailyTrainingHandler())
sb.add_request_handler(StartSheetHandler())
sb.add_request_handler(SkipExerciseHandler())
sb.add_request_handler(EndRepsHandler())

sb.add_request_handler(HelpIntentHandler())
sb.add_request_handler(CancelOrStopIntentHandler())
sb.add_request_handler(SessionEndedRequestHandler())
sb.add_request_handler(IntentReflectorHandler()) # IntentReflectorHandler is the last add_request_handler
sb.add_exception_handler(CatchAllExceptionHandler())

skill = sb.create()
