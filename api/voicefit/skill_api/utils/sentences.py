import random
from ...utils.funcs import is_female

def randElement(arr):
    return arr[random.randint(0, len(arr)-1)]

def help_msg():
    msg = "Nella skill voice fit puoi richiedere di conoscere la scheda che ti è stata assegnata dal personal trainer chiedendo \"qual'è la mia scheda\", puoi richiedere di eseguirla insieme ad alexa dicendo \"iniziamo gli esercizi\" oppure puoi richiedere la lettura di un report con le statistiche dicendo \"leggi le statistiche\""
    return msg
    
# welcome message
def hello_msg(name, gender):
    if is_female(gender):
        msgs = [
            "Ciao " + name + ", benvenuta nella skill voice fit",
            name + ", benvenuta in voice fit",
            "Ben arrivata in voice fit " + name
        ]
    else:
        msgs = [
            "Ciao " + name + " benvenuto nella skill voice fit",
            name + ", benvenuto in voice fit",
            "Ben arrivato in voice fit " + name
        ]
    return randElement(msgs)

def completed_sheet_msg():
    msgs = [
       "non ci sono altre schede da completare oggi, prova domani",
       "hai terminato tutti gli esercizi di oggi",
       "il personal trainer non ha previsto altri esercizi per oggi"
    ]
    return randElement(msgs)

def no_sheet_msg():
    msgs = [
       "non ci sono schede da completare oggi, prova domani",
       "il personal trainer non ha previsto esercizi per oggi"
    ]
    return randElement(msgs)

def yes_sheet_msg():
    msgs = [
        "c'è una scheda da svolgere",
        "la scheda prevista per oggi è ancora da svolgere"
    ]
    return randElement(msgs)

# check sheet
def intro_sheet_msg(sheet_name, duration, exercises_count):
    exercises_word = "esercizio" if exercises_count == 1 else "esercizi" 
    if sheet_name != None and sheet_name != "":
        msgs = [
            "la scheda di oggi si intitola " + str(sheet_name) + ", è composta da " + str(exercises_count) + " " + str(exercises_word) + " ed ha una durata prevista di " + str(duration) + " minuti",
            "oggi la scheda è " + str(sheet_name) + ", dura circa " + str(duration) + " minuti ed è composta da " + str(exercises_count) + " " + str(exercises_word)
        ]
    else:
        msgs = [
            "la scheda di oggi ha una durata prevista di " + str(duration) + " ed è composta da " + str(exercises_count) + " " + str(exercises_word),
            "oggi la scheda dura circa " + str(duration) + "ed è composta da " + str(exercises_count) + " " + str(exercises_word)
        ]
    return randElement(msgs)

def compliments_msg(gender):
    if is_female(gender):
        msgs = [
            "ottimo",
            "brava",
            "grandioso",
            "perfetto",
            "ben fatto",
            "continua così"
        ]
    else:
        msgs = [
            "ottimo",
            "bravo",
            "grandioso",
            "perfetto",
            "ben fatto",
            "continua così"
        ]
    return randElement(msgs)

def finish_serie_msg(gender):
    msgs = [
        "andiamo avanti con le serie",
        "proseguiamo con la prossima serie",
        "proseguiamo",
        "passiamo alla prossima serie"
    ]
    return compliments_msg(gender) + ', ' + randElement(msgs)

def finish_exercise_msg(gender):
    msgs = [
        "andiamo avanti con il prossimo esercizio",
        "proseguiamo con l'esercizio successivo",
        "passiamo al prossimo esercizio"
    ]
    return compliments_msg(gender) + ', ' + randElement(msgs)

def finish_sheet_msg(gender):
    msgs = [
        "con questo abbiamo terminato la scheda, torna domani",
        "con questo abbiamo terminato, torna domani per conoscere la nuova scheda",
        "per oggi abbiamo terminato, torna domani per sapere quali sono i nuovi esercizi",
        "per oggi abbiamo concluso, torna domani per un nuovo allenamento"
    ]
    return compliments_msg(gender) + ', ' + randElement(msgs)

# start  exercise
def init_exercise(exercise_name, reps, gender):
    if(is_female(gender)):
        msgs = [
            "adesso esegui " + reps + " ripetizioni di " + exercise_name,
            "preparati: esegui ora "  + reps + " ripetizioni di " + exercise_name,
            "sei pronta? esegui " + reps + " ripetizioni di " + exercise_name
        ]
    else:
        msgs = [
            "adesso esegui " + reps + " ripetizioni di " + exercise_name,
            "preparati: esegui ora "  + reps + " ripetizioni di " + exercise_name,
            "sei pronto? esegui " + reps + " ripetizioni di " + exercise_name
        ]
    return randElement(msgs)

# read exercises
def exercises_sheet_msg(index, name, n_serie, serie):
    msg = index + ": " + str(n_serie) + ' serie di ' + name + " da :"
    for s in serie:
        msg += s + ", "
    msg = msg[:-2]
    msg += " ripetizioni"
    return msg

def started_sheet_msg():
    msg = "La scheda di oggi è già cominciata."
    return msg

def not_started_sheet_msg():
    msgs = [
        "Quando sei pronto puoi avviare la scheda",
        "Per avviare la scheda puoi utilizzare il comando iniziamo"
    ]
    return randElement(msgs)