import hashlib

def sha3_hash(plain_key):
    return hashlib.sha3_512(bytearray(plain_key, 'utf-8')).hexdigest()