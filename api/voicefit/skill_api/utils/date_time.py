from datetime import datetime
import pytz

def getReqDate(handler_input, d_format):
    req_now = handler_input.request_envelope.request.timestamp
    locale = handler_input.request_envelope.request.locale
    locale = locale[:2]
    loc_timezone = pytz.timezone(pytz.country_timezones(locale)[0])
    date_now = req_now.astimezone(loc_timezone)
    date_now = datetime.strftime(date_now, d_format)
    return date_now