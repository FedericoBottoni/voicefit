import ask_sdk_core.utils as ask_utils
import logging
import requests
from datetime import datetime
import pytz
from tzlocal import get_localzone
from ask_sdk_core.dispatch_components import AbstractRequestHandler
from ask_sdk_core.handler_input import HandlerInput
from ask_sdk_core.utils import is_intent_name
from ask_sdk_model.response import Response
from ask_sdk_model.ui import AskForPermissionsConsentCard
from ask_sdk_model.services import ServiceException
from ..alexa_api_methods import setCustomer, getCustomerInfo, getTrainingSheetInfo, startSheet, skipExercise, endReps
from .utils.sentences import hello_msg, completed_sheet_msg, no_sheet_msg, yes_sheet_msg, intro_sheet_msg, exercises_sheet_msg, init_exercise, finish_exercise_msg, finish_sheet_msg, started_sheet_msg, not_started_sheet_msg, finish_serie_msg
from .utils.sleepingBabies import sha3_hash
from .utils.date_time import getReqDate

logger = logging.getLogger(__name__)

class LaunchRequestHandler(AbstractRequestHandler):
    """Handler for Skill Launch."""
    def can_handle(self, handler_input):
        # type: (HandlerInput) -> bool

        return ask_utils.is_request_type("LaunchRequest")(handler_input)

    def handle(self, handler_input):
        '''
        token = handler_input.request_envelope.context.system.api_access_token
        api_end_point = handler_input.request_envelope.context.system.api_endpoint
        headers = {
            "Host": "api.amazonalexa.com",
            "Accept": "application/json",
            "Authorization": "Bearer {}".format(token)}

        resp = requests.get('{api_end_point}/v2/accounts/~current/settings/Profile.email'.format(api_end_point=api_end_point),headers=headers)
        if resp.status_code == 200:
            print(resp.json())
        else:
            print(resp)
        '''
        '''
        req_envelope = handler_input.request_envelope
        service_client_fact = handler_input.service_client_factory
        response_builder = handler_input.response_builder
        if not (req_envelope.context.system.user.permissions and req_envelope.context.system.user.permissions.consent_token):
            response_builder.speak("Please enable profile permissions in the Amazon Alexa app.")
            response_builder.set_card(AskForPermissionsConsentCard(permissions=["alexa::profile:email:read"]))
            return response_builder.response
        else:
            logger.info("In LaunchRequest user found")
        try:
            service_client_fact = handler_input.service_client_factory
            device_email_client = service_client_fact.get_ups_service()
            email = device_email_client.get_profile_email()
            handler_input.attributes_manager.session_attributes['profileName'] = device_email_client.get_profile_name()
            handler_input.attributes_manager.session_attributes['email'] = email
            logger.info("email found " + str (email))
            cid = sha3_hash(email)
        except Exception as e:
            logger.info(str(e))
        raise e
        '''
        date_now = getReqDate(handler_input, '%Y-%m-%d')
        print(date_now)
        csk = sha3_hash(handler_input.request_envelope.session.session_id)
        print(csk)
        #cid = sha3_hash(handler_input.service_client_factory.get_ups_service().get_profile_email())
        ### TO REMOVE ASAP ###
        cid = 1
        ######################
        customerSet = setCustomer(customer_id = cid, customer_session_key = csk, date = date_now)
        if customerSet:
            c = getCustomerInfo(customer_session_key = csk)
            if c["completed"] == "None":
                sheet_msg = no_sheet_msg()
            elif c["completed"] == "True":
                sheet_msg = completed_sheet_msg()
            else:
                sheet_msg = yes_sheet_msg()
            speak_output = hello_msg(c["name"], c["gender"]) + ". " + sheet_msg
        else:
            speak_output = no_sheet_msg()
        return (
            handler_input.response_builder
                .speak(speak_output)
                .ask(speak_output)
                .response
        )


class DailyTrainingHandler(AbstractRequestHandler):
    def can_handle(self, handler_input):
        # type: (HandlerInput) -> bool
        return ask_utils.is_intent_name("DailyTraining")(handler_input)

    def handle(self, handler_input):
        now = getReqDate(handler_input, '%Y-%m-%d %H:%M:%S')
        print("now is: " + now)
        csk = sha3_hash(handler_input.request_envelope.session.session_id)
        c = getCustomerInfo(customer_session_key = csk)
        if c["completed"] == "None":
            speak_output = no_sheet_msg()
        elif c["completed"] == "True":
            speak_output = completed_sheet_msg()
        else:
            s = getTrainingSheetInfo(customer_session_key = csk)
            exercises_count = len(s["exercises"].keys())
            speak_output = intro_sheet_msg(s["sheet_name"], s["duration"], exercises_count) + ". "
            for e in sorted(s["exercises"].keys()):
                serie = s["exercises"][e]["repetitions"].split(' ')
                speak_output += "; " + exercises_sheet_msg(e, s["exercises_names"][e], len(serie), serie) + ". "

        return (
            handler_input.response_builder
                .speak(speak_output)
                .ask(speak_output)
                .response
        )


class StartSheetHandler(AbstractRequestHandler):
    def can_handle(self, handler_input):
        # type: (HandlerInput) -> bool
        return ask_utils.is_intent_name("StartSheet")(handler_input)

    def handle(self, handler_input):
        now = getReqDate(handler_input, '%Y-%m-%d %H:%M:%S')
        print("now is: " + now)
        csk = sha3_hash(handler_input.request_envelope.session.session_id)
        c = getCustomerInfo(customer_session_key = csk)
        if c["completed"] == "None":
            speak_output = no_sheet_msg()
        elif c["completed"] == "True":
            speak_output = completed_sheet_msg()
        elif c["started"] == "True":
            speak_output = started_sheet_msg()
        else:
            s = getTrainingSheetInfo(customer_session_key = csk)
            r = startSheet(csk, now)
            print(r)
            if r == {}:
                speak_output = no_sheet_msg()
            else:
                exercises_count = len(s["exercises"].keys())
                speak_output = intro_sheet_msg(s["sheet_name"], s["duration"], exercises_count) + ". "
                reps = s["exercises"][r["order_id"]]["repetitions"].split(' ')[int(r["reps_id"])-1]
                speak_output += init_exercise(s["exercises_names"][r["order_id"]], reps, c["gender"]) + ". "
        return (
            handler_input.response_builder
                .speak(speak_output)
                .ask(speak_output)
                .response
        )


class SkipExerciseHandler(AbstractRequestHandler):
    def can_handle(self, handler_input):
        # type: (HandlerInput) -> bool
        return ask_utils.is_intent_name("SkipExercise")(handler_input)

    def handle(self, handler_input):
        now = getReqDate(handler_input, '%Y-%m-%d %H:%M:%S')
        print("now is: " + now)
        csk = sha3_hash(handler_input.request_envelope.session.session_id)
        c = getCustomerInfo(customer_session_key = csk)
        if c["completed"] == "None":
            speak_output = no_sheet_msg()
        elif c["completed"] == "True":
            speak_output = completed_sheet_msg()
        elif c["started"] == "False":
            speak_output = not_started_sheet_msg()
        else:
            s = getTrainingSheetInfo(customer_session_key = csk)
            r = skipExercise(csk, now)
            print(r)
            if r == {}:
                speak_output = finish_sheet_msg(c["gender"])
            else:
                reps = s["exercises"][r["order_id"]]["repetitions"].split(' ')[int(r["reps_id"])-1]
                speak_output = init_exercise(s["exercises_names"][r["order_id"]], reps, c["gender"]) + ". "
        return (
            handler_input.response_builder
                .speak(speak_output)
                .ask(speak_output)
                .response
        )


class EndRepsHandler(AbstractRequestHandler):
    def can_handle(self, handler_input):
        # type: (HandlerInput) -> bool
        return ask_utils.is_intent_name("EndReps")(handler_input)

    def handle(self, handler_input):
        now = getReqDate(handler_input, '%Y-%m-%d %H:%M:%S')
        print("now is: " + now)
        csk = sha3_hash(handler_input.request_envelope.session.session_id)
        c = getCustomerInfo(customer_session_key = csk)
        if c["completed"] == "None":
            speak_output = no_sheet_msg()
        elif c["completed"] == "True":
            speak_output = completed_sheet_msg()
        elif c["started"] == "False":
            speak_output = not_started_sheet_msg()
        else:
            s = getTrainingSheetInfo(customer_session_key = csk)
            # TODO: implement timer
            r = endReps(csk, now)
            print(r)
            if r == {}:
                speak_output = finish_sheet_msg(c["gender"])
            else:
                if(r["reps_id"] == "1"):
                    speak_output = finish_exercise_msg(c["gender"]) + ". "
                else:
                    speak_output = finish_serie_msg(c["gender"]) + ". "
                reps = s["exercises"][r["order_id"]]["repetitions"].split(' ')[int(r["reps_id"])-1]
                speak_output += init_exercise(s["exercises_names"][r["order_id"]], reps, c["gender"]) + ". "
        return (
            handler_input.response_builder
                .speak(speak_output)
                .ask(speak_output)
                .response
        )