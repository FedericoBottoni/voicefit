from http.server import BaseHTTPRequestHandler
import socketserver
from io import BytesIO
from datetime import datetime, time, timedelta
import pytz
from tzlocal import get_localzone
import json
import threading
'''
import copy
'''

sessionMem = dict()

'''
# At the beginning of a new session is necessary to look for possible previous crashed sessions. If so, the training should begin where left
# Crashed customers must be saved at the end of the day as partially completed, so it's then possible to clean ram space for the next day
# This last step is not mandatory since the cache is refreshed at the beginning of a new session, but helps to keep ram usage low
# Idea: at 23:59 deepcopy sessionMem somewhere and manually set everyone as ended

def set_clean_delay(now = datetime.now()):
    delta_t = datetime.combine(now.date(), time(23, 59, 0, 0)) - now
    delta_t_seconds = delta_t.total_seconds()
    threading.Timer(delta_t_seconds, save_all).start()

def save_all():
    to_clean = copy.deepcopy(sessionMem)
    #TODO implement cleaning here
'''

def setDumpDelay():
    now = datetime.now()
    delta_t = datetime.combine(datetime.now().date() + timedelta(days=1), time(23, 59, 0, 0)) - now
    delta_t_seconds = delta_t.total_seconds()
    print(delta_t_seconds)
    threading.Timer(delta_t_seconds, dump_all).start()

def dump_all():
    now = datetime.strftime(datetime.now(), '%Y-%m-%d_%H-%M-%S')
    with open('/var/voicefit_dumps/dump_' + now + '.json', 'w+') as outfile:
        json.dump(sessionMem, outfile)
    setDumpDelay()

def sendResponse(req, msg = "{}"):
    req.send_response(200)
    req.end_headers()
    response = BytesIO()
    response.write(bytearray(msg, 'utf-8'))
    req.wfile.write(response.getvalue())

def sendError(req, msg):
    req.send_response(200)
    req.end_headers()
    response = BytesIO()
    response.write(bytearray('{"False": "' + msg + '"}', 'utf-8'))
    req.wfile.write(response.getvalue())

class MyRequestHandler(BaseHTTPRequestHandler):
    
    def do_POST(self):
        print(sessionMem)
        content_length = int(self.headers['Content-Length'])
        body = self.rfile.read(content_length)
        data = json.loads(body.decode('utf-8'))
        method = self.path.split("/")[1]
        if method.startswith('setUser'):
            if data["user"] not in sessionMem:
                sessionMem[data["user"]] = dict()
                sessionMem[data["user"]]["date"] = data["attributes"]["date"]
                sessionMem[data["user"]]["session"] = data["attributes"]["session"]
                sessionMem[data["attributes"]["session"]] = data["user"]
                print("Cached non existing user: " + data["user"])
                sendResponse(self)
            else:
                sessionMem.pop(sessionMem[data["user"]]["session"])
                sessionMem[data["user"]]["session"] = data["attributes"]["session"]
                sessionMem[data["attributes"]["session"]] = data["user"]
                last_date = sessionMem[data["user"]]["date"]
                sessionMem[data["user"]]["date"] = data["attributes"]["date"]
                print("Cached existing user: " + data["user"])
                sendResponse(self, '{"date": "' + last_date + '"}')
        elif method.startswith('getUser'):
            if data["session"] in sessionMem:
                print("Returning cached user id: " + sessionMem[data["session"]] + "; for session key: " + data["session"])
                sendResponse(self, '{"user": "' + sessionMem[data["session"]] + '"}')
            else:
                sendError(self, "user doesn't exists")
        elif method.startswith('setAttribute'):
            if data["user"] in sessionMem:
                for attr in data["attributes"].keys():
                    sessionMem[data["user"]][attr] = data["attributes"][attr]
                print("Attributes set for user: " + data["user"])
                sendResponse(self)
            else:
                sendError(self, "user doesn't exists")
        elif method.startswith('getAttribute'):
            if data["user"] in sessionMem:
                error = False
                res = dict()
                for attr in data["attributes"]:
                    if attr in sessionMem[data["user"]]:
                        res[attr] = sessionMem[data["user"]][attr]
                    else:
                        error = True
                        break
                if(error):
                    sendError(self, "attribute doesn't exists")
                else:
                    print("Returning attributes: " + json.dumps(res))
                    sendResponse(self, json.dumps(res))
            else:
                sendError(self, "user doesn't exists")
        else:
            sendError(self, "method not implemented")

ADDRESS = "127.0.0.1"
PORT = 53871

'''
## It's necessary to check possible datetime changes due to timezone standars
## Idea: check time at 12:00 and create a delay from there to needed datetime
# get datetime
now = datetime.now()
# create datetime with actual date and 12:00 time
twelve = datetime.combine(now.date(), time(12, 0, 0, 0))
# if time is less than 12:00
if(now < twelve):
    # calculate time delay
    delta_t = (twelve - now)
    # calculate time delay in seconds
    delta_t_seconds = delta_t.total_seconds()
    # set a thred-timer that trigger after delay in seconds
    # thred-timers also work with negative delay integers (causing an instant triggering), so acceptable computational delays don't affect the timing (cannot happend here since the one-time measurement)
    threading.Timer(delta_t_seconds, set_clean_delay).start()
# if time is more than 12:00
else:
    # call the delay function with actual datetime to avoid computational delays and related possible date-change
    set_clean_delay(now = now)
'''

setDumpDelay()

with socketserver.TCPServer((ADDRESS, PORT), MyRequestHandler) as httpd:
    print("Serving at port: ", PORT)
    httpd.serve_forever()