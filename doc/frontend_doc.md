## API

<dl>
<dt><a href="#module_getExercises">getExercises</a></dt>
<dd><p>Allows to gather every information about the existing exercises</p>
</dd>
<dt><a href="#module_getSheets">getSheets</a></dt>
<dd><p>Allows to gather every information about the existing sheets</p>
</dd>
<dt><a href="#module_getSheets.module_assignSheet">assignSheet</a></dt>
<dd><p>Perform the assignation of a customers set, in the specified dates, to the specified sheet</p>
</dd>
<dt><a href="#module_getSheets.module_assignSheet.module_createSheet">createSheet</a></dt>
<dd><p>Create a new sheet from the passed object</p>
</dd>
<dt><a href="#module_getSheets.module_assignSheet.module_createSheet.module_updateSheet">updateSheet</a></dt>
<dd><p>Updated the specified sheet from the passed object</p>
</dd>
<dt><a href="#module_getSheets.module_assignSheet.module_createSheet.module_updateSheet.module_deleteSheet">deleteSheet</a></dt>
<dd><p>Delete a sheet from its id</p>
</dd>
<dt><a href="#module_getCustomers">getCustomers</a></dt>
<dd><p>Gather each customer</p>
</dd>
<dt><a href="#module_getCustomers.module_getAvailableCustomers">getAvailableCustomers</a></dt>
<dd><p>Gather customers filtering the availables in the passed dates set</p>
</dd>
</dl>

## Mixins

<dl>
<dt><a href="#uuidv4">uuidv4</a></dt>
<dd><p>Function that generates a random GUID</p>
</dd>
</dl>

## Constants

<dl>
<dt><a href="#DEFAULT_ERROR_MSG">DEFAULT_ERROR_MSG</a></dt>
<dd><p>Default error message for API calls</p>
</dd>
</dl>

## Functions

<dl>
<dt><a href="#CreateSheet">CreateSheet(setDefaultSnackbarError, createOpen, onDialogClose)</a></dt>
<dd><p>This component manages the creation of a new sheets</p>
</dd>
<dt><a href="#DeleteConfirmDialog">DeleteConfirmDialog(sheetToDelete, onDeleteConfirm, onClose)</a></dt>
<dd><p>The component is a dialog which require the confirmation of the sheet deletion from the user</p>
</dd>
<dt><a href="#SheetDialog">SheetDialog(sheet, onClose)</a></dt>
<dd><p>The component is a dialog which require the confirmation of the sheet deletion from the user</p>
</dd>
<dt><a href="#ExerciseDetails">ExerciseDetails(exercises, sheetName, duration, resetPageState, setDefaultSnackbarError, setExercises, onClose)</a></dt>
<dd><p>This component manages the creation of a new sheets</p>
</dd>
<dt><a href="#ExerciseItem">ExerciseItem(index, schema, repetitionsSplitted, cooldownsSplitted)</a></dt>
<dd><p>This component renders the name of the exercise and the sliders</p>
</dd>
<dt><a href="#ExercisePaper">ExercisePaper(series, moveCard, setSerie, setSeries, deleteSerie, ItemTypes, exercise, index, dragItem, setDragItem, onDrop)</a></dt>
<dd><p>This component gives the details of a new sheets</p>
</dd>
<dt><a href="#SheetList">SheetList(selectedSheet, setSelectedSheet, setSelectedSheetInfo, setDefaultSnackbarError)</a></dt>
<dd><p>This component renders the list of the avaiable list in a pre-defined set of dates</p>
</dd>
<dt><a href="#TransferList">TransferList(title, initLeft, initRight, getStateCallback, getState)</a></dt>
<dd><p>This component renders the list of the avaiable list in a pre-defined set of dates</p>
</dd>
</dl>

<a name="module_getExercises"></a>

## getExercises
Allows to gather every information about the existing exercises

<a name="module_getSheets"></a>

## getSheets
Allows to gather every information about the existing sheets

<a name="module_getSheets.module_assignSheet"></a>

## assignSheet
Perform the assignation of a customers set, in the specified dates, to the specified sheet

<a name="module_getSheets.module_assignSheet.module_createSheet"></a>

## createSheet
Create a new sheet from the passed object

<a name="module_getSheets.module_assignSheet.module_createSheet.module_updateSheet"></a>

## updateSheet
Updated the specified sheet from the passed object

<a name="module_getSheets.module_assignSheet.module_createSheet.module_updateSheet.module_deleteSheet"></a>

## deleteSheet
Delete a sheet from its id

<a name="module_getCustomers"></a>

## getCustomers
Gather each customer

<a name="module_getCustomers.module_getAvailableCustomers"></a>

## getAvailableCustomers
Gather customers filtering the availables in the passed dates set

<a name="uuidv4"></a>

## uuidv4
Function that generates a random GUID

**Kind**: global mixin  
<a name="DEFAULT_ERROR_MSG"></a>

## DEFAULT\_ERROR\_MSG
Default error message for API calls

**Kind**: global constant  
<a name="CreateSheet"></a>

## CreateSheet(setDefaultSnackbarError, createOpen, onDialogClose)
This component manages the creation of a new sheets

**Kind**: global function  

| Param | Description |
| --- | --- |
| setDefaultSnackbarError | Callback which fires the snackbar error message |
| createOpen | Controls the role of the dialog: can be a TRUE if the dialog is open in created-mod, an object if it's in update-mod and FALSE if it's closed |
| onDialogClose | Callback which fires the dialog closure |

<a name="CreateSheet..exerciseListCallback"></a>

### CreateSheet~exerciseListCallback()
This state is used to store the exercises

**Kind**: inner method of [<code>CreateSheet</code>](#CreateSheet)  
<a name="DeleteConfirmDialog"></a>

## DeleteConfirmDialog(sheetToDelete, onDeleteConfirm, onClose)
The component is a dialog which require the confirmation of the sheet deletion from the user

**Kind**: global function  

| Param | Description |
| --- | --- |
| sheetToDelete | Object that controls the dialog: if it exists the dialog is open and it is filled by its attributes, otherwise it's closed |
| onDeleteConfirm | Callback fired on the confirm button click, it concerns the api call and the deletion |
| onClose | Callback which fires the dialog closure |

<a name="SheetDialog"></a>

## SheetDialog(sheet, onClose)
The component is a dialog which require the confirmation of the sheet deletion from the user

**Kind**: global function  

| Param | Description |
| --- | --- |
| sheet | Object that controls the dialog: if it exists the dialog is open and it is filled by its attributes, otherwise it's closed |
| onClose | Callback which fires the dialog closure |

<a name="ExerciseDetails"></a>

## ExerciseDetails(exercises, sheetName, duration, resetPageState, setDefaultSnackbarError, setExercises, onClose)
This component manages the creation of a new sheets

**Kind**: global function  

| Param | Description |
| --- | --- |
| exercises | Object that contains the list of exercises |
| sheetName | Object that contains the name of the sheet |
| duration | Object that contains the duration of the sheet |
| resetPageState | Callback that triggers the reset of the page state |
| setDefaultSnackbarError | Callback which fires the snackbar error message |
| setExercises | Used to set the Exercises in case of a Drop event |
| onClose | Handler of the Close event |

<a name="ExerciseItem"></a>

## ExerciseItem(index, schema, repetitionsSplitted, cooldownsSplitted)
This component renders the name of the exercise and the sliders

**Kind**: global function  

| Param | Description |
| --- | --- |
| index | Object that constain the index of the selected exercise |
| schema | Object that constain the name of the selected exercise |
| repetitionsSplitted | Object that contains the repetitions of the exercises after they have been splitted |
| cooldownsSplitted | Object that contains the cooldowns of the exercises after they have been splitted |

<a name="ExercisePaper"></a>

## ExercisePaper(series, moveCard, setSerie, setSeries, deleteSerie, ItemTypes, exercise, index, dragItem, setDragItem, onDrop)
This component gives the details of a new sheets

**Kind**: global function  

| Param | Description |
| --- | --- |
| series | Object containing all the info about the series |
| moveCard | Event that triggers when a card is moved |
| setSerie | Callback used to edit the serie state of a single element |
| setSeries | Callback used to edit the serie state of multiple element |
| deleteSerie | Callback used to manage the remotion of a serie |
| ItemTypes | Object containing the Exercise item type |
| exercise | Object containing all the exercises |
| index | Object containing the index of the dragged item |
| dragItem | Object containing the dragged item |
| setDragItem | Callback used to set the dragged item state |
| onDrop | Event that manages the drop |

<a name="SheetList"></a>

## SheetList(selectedSheet, setSelectedSheet, setSelectedSheetInfo, setDefaultSnackbarError)
This component renders the list of the avaiable list in a pre-defined set of dates

**Kind**: global function  

| Param | Description |
| --- | --- |
| selectedSheet | Object containing the id of the selected sheet |
| setSelectedSheet | Callback used to set the selected sheet id |
| setSelectedSheetInfo | Callback used to set the selected sheet object |
| setDefaultSnackbarError | Callback which fires the snackbar error message |

<a name="TransferList"></a>

## TransferList(title, initLeft, initRight, getStateCallback, getState)
This component renders the list of the avaiable list in a pre-defined set of dates

**Kind**: global function  

| Param | Description |
| --- | --- |
| title | Object that contains the title of the sheet |
| initLeft | Object that contains a list of elements that will be added to the left list |
| initRight | Object that contains a list of elements that will be added to the right list |
| getStateCallback | Callback which set the left and right states |
| getState | Object that trigger the getStateCallback |

