
## Alexa API

<dl>
<dt><a href="#method_LaunchRequest">LaunchRequest</a></dt>
<dd><p>Allows to create a new training session for a specific user</p>
</dd>
<dt><a href="#method_DailyTraining">DailyTraining</a></dt>
<dd><p>Allows to gather every information about the daily sheet</p>
</dd>
<dt><a href="#method_StartSheet">StartSheet</a></dt>
<dd><p>Allows to start the daily sheet for the current Customer</p>
</dd>
<dt><a href="#method_SkipExercise">SkipExercise</a></dt>
<dd><p>Allows to skip the current exercise</p>
</dd>
<dt><a href="#method_EndReps">EndReps</a></dt>
<dd><p>Allows to proceed to the next serie</p>
</dd>
</dl>

## REST API

<dl>
<dt>customer_list</dt>
<dd><p>Return the customer list of the logged trainser</p>
</dd>
<dt>exercise_schema_list</dt>
<dd><p>Return the exercise schema list</p>
</dd>
<dt>sheet_list</dt>
<dd><p>Return the sheets created by the logged trainer</p>
</dd>
<dt>available_customers</dt>
<dd><p>Return the list of available customers of the logged trainer in the specified dates</p>
</dd>
<dt>create_sheet</dt>
<dd><p>Add a new sheet record in the Sheet table</p>
</dd>
<dt>assign_sheet</dt>
<dd><p>Assign a sheet to the customers specified by the logged trainer</p>
</dd>
<dt>delete_sheet</dt>
<dd><p>Delete the selected sheets</p>
</dd>
<dt>update_sheet</dt>
<dd><p>Update attibutes of the selected sheet, keeping the consistency of the past</p>
</dd>
</dl>


## Constants

<dl>
<dt><a href="#FULL_DATE">FULL_DATE</a></dt>
<dd><p>Default date iso format</p>
</dd>
<dt><a href="#FULL_TIME">FULL_TIME</a></dt>
<dd><p>Default time iso format</p>
</dd>
<dt><a href="#FULL_DATE_TIME">FULL_DATE_TIME</a></dt>
<dd><p>Default date-time iso format</p>
</dd>
</dl>

## Methods

<dl>
<dt><a name="method_LaunchRequest" href="#LaunchRequestHandler">LaunchRequestHandler.handle(self, handler_input)</a></dt>
<dd><p>This method allows to create a new training session for a specific user</p>
</dd>
<dt><a name="method_DailyTraining" href="#DailyTrainingHandler">DailyTrainingHandler.handle(self, handler_input)</a></dt>
<dd><p>This method allows to gather every information about the daily sheet</p>
</dd>
<dt><a name="method_StartSheet" href="#StartSheetHandler">StartSheetHandler.handle(self, handler_input)</a></dt>
<dd><p>This method allows to start the daily sheet for the current Customer</p>
</dd>
<dt><a name="method_SkipExercise" href="#SkipExerciseHandler">SkipExerciseHandler.handle(self, handler_input)</a></dt>
<dd><p>This method allows to skip the current exercise</p>
</dd>
<dt><a name="method_EndReps" href="#EndRepsHandler">EndRepsHandler.handle(self, handler_input)</a></dt>
<dd><p>This method allows to proceed to the next serie</p>
</dd>
<dt><a name="method_getCustomer" href="#getCustomer">getCustomer(customer_key)</a></dt>
<dd><p>This method allows get the current Customer Django object</p>
</dd>
<dt><a name="method_refreshSheetCache" href="#refreshSheetCache">refreshSheetCache(customer_key, customer_gender, customer_name, completed, sheet_id, sheet_name, duration, exercises, exercises_names)</a></dt>
<dd><p>This method allows set the cache with a customer sheet parameters</p>
</dd>
<dt><a name="method_setCustomer" href="#setCustomer">setCustomer(customer_id, customer_session_key, date)</a></dt>
<dd><p>This method allows to initialize a Customer session parameters inside the cache</p>
</dd>
<dt><a name="method_getCustomerInfo" href="#getCustomerInfo">getCustomerInfo(customer_session_key)</a></dt>
<dd><p>This method allows to get the Customer parameters stored inside the cache</p>
</dd>
<dt><a name="method_getTrainingSheetInfo" href="#getTrainingSheetInfo">getTrainingSheetInfo(customer_session_key)</a></dt>
<dd><p>This method allows to get the current Sheet parameters stored inside the cache</p>
</dd>
<dt><a name="method_getTodaySheetCustomer" href="#getTodaySheetCustomer">getTodaySheetCustomer(customer_key)</a></dt>
<dd><p>This method allows to get the SheetCustomer Django object associated to the current sheet</p>
</dd>
<dt><a name="method_getTrainingSheet" href="#getTrainingSheet">getTrainingSheet(customer_session_key)</a></dt>
<dd><p>This method allows to get the current sheet parameters from the DB</p>
</dd>
<dt><a name="method_startSheet" href="#startSheet">startSheet(customer_session_key, date_time)</a></dt>
<dd><p>This method allows to set the current sheet and first exercise states as started inside the cache and DB</p>
</dd>
<dt><a name="method_startExercise" href="#startExercise">startExercise(customer_session_key, date_time)</a></dt>
<dd><p>This method allows to set the next exercise state as started and the last one as ended inside the cache and DB</p>
</dd>
<dt><a name="method_skipExercise" href="#skipExercise">skipExercise(customer_session_key, date_time)</a></dt>
<dd><p>This method allows to set the next exercise state as started and the last one as skipped inside the cache and DB</p>
</dd>
<dt><a name="method_endReps" href="#endReps">endReps(customer_session_key, date_time)</a></dt>
<dd><p>This method allows to set the next exercise serie as started and the last one as ended inside the cache</p>
</dd>
<dt><a name="method_endSheet" href="#endSheet">endSheet(customer_session_key, date_time)</a></dt>
<dd><p>This method allows to set the current exercise and sheet as ended inside the cache and DB</p>
</dd>
</dd>
<dt><a href="#returnSheets">return_sheets(trainer)</a></dt>
<dd><p>Return the list of the sheets created by the logged trainer</p>
</dd>
</dd>
<dt><a href="#createSheet">create(trainer_id, sheet_name, duration, order, exercise_schema_id, repetitions, cooldowns, notes)</a></dt>
<dd><p>Add a new Sheet record and the relative exercises in the database
</p>
</dd>
</dl>


<a name="LaunchRequestHandler"></a>
## LaunchRequestHandler.handle(self, handler_input)
This method allows to create a new training session for a specific user

**Kind**: global function  

| Param | Description |
| --- | --- |
| self | ASK SDK LaunchRequestHandler current istance |
| handler_input | object containing an instance for ASK SDK Request Handler, Exception Handler and Interceptors |

**Return**: ASK SDK Response Builder serialization including a String containing a welcome message

<a name="DailyTrainingHandler"></a>
## DailyTrainingHandler.handle(self, handler_input)
This method allows to gather every information about the daily sheet

**Kind**: global function  

| Param | Description |
| --- | --- |
| self | ASK SDK DailyTrainingHandler current istance |
| handler_input | object containing an instance for ASK SDK Request Handler, Exception Handler and Interceptors |

**Return**: ASK SDK Response Builder serialization including a String containing the serialized daily sheet

<a name="StartSheetHandler"></a>
## StartSheetHandler.handle(self, handler_input)
This method allows to start the current sheet of the day for a specific user

**Kind**: global function  

| Param | Description |
| --- | --- |
| self | ASK SDK StartSheetHandler current istance |
| handler_input | object containing an instance for ASK SDK Request Handler, Exception Handler and Interceptors |

**Return**: ASK SDK Response Builder serialization including a String containing the serialized first serie of the first exercise of the daily sheet

<a name="SkipExerciseHandler"></a>
## SkipExerciseHandler.handle(self, handler_input)
This method allows to skip the current exercise

**Kind**: global function  

| Param | Description |
| --- | --- |
| self | ASK SDK SkipExerciseHandler current istance |
| handler_input | object containing an instance for ASK SDK Request Handler, Exception Handler and Interceptors |

**Return**: ASK SDK Response Builder serialization including a String containing the serialized first serie of the next exercise of the daily sheet. If the current sheet just ended, the string will be an outro message.

<a name="EndRepsHandler"></a>
## EndRepsHandler.handle(self, handler_input)
This method allows to proceed to the next serie

**Kind**: global function  

| Param | Description |
| --- | --- |
| self | ASK SDK EndRepsHandler current istance |
| handler_input | object containing an instance for ASK SDK Request Handler, Exception Handler and Interceptors |

**Return**: ASK SDK Response Builder serialization including a String containing the serialized next serie of the current exercise of the daily sheet. If the current exercise just ended, the string will be the first serie of the next exercise.

<a name="getCustomer"></a>
## getCustomer(customer_key)
This method allows get the current Customer Django object

**Kind**: global function  

| Param | Description |
| --- | --- |
| customer_key | customer DB id |

**Return**: Django Customer object

<a name="refreshSheetCache"></a>
## refreshSheetCache(customer_key, customer_gender, customer_name, completed, sheet_id, sheet_name, duration, exercises, exercises_names)
This method allows set the cache with a customer sheet parameters

**Kind**: global function  

| Param | Description |
| --- | --- |
| customer_key | customer DB id value |
| customer_name | customer DB name value |
| completed | customer DB completed value |
| sheet_id | sheet DB id value |
| sheet_name | sheet DB name value |
| duration | sheet DB duration value |
| exercises | dictionary containing the current sheet DB exercises series repetitions, cooldowns and notes |
| exercises_names | dictionary containing the current sheet DB exercises names |

**Return**: None

<a name="setCustomer"></a>
## setCustomer(customer_id, customer_session_key, date)
This method allows to initialize a Customer session parameters inside the cache

**Kind**: global function  

| Param | Description |
| --- | --- |
| customer_id | customer DB id value |
| customer_session_key | customer session id value |
| date | session date in String format |

**Return**: True or False if the set action failed somehow

<a name="getCustomerInfo"></a>
## getCustomerInfo(customer_session_key)
This method allows to get the Customer parameters stored inside the cache

**Kind**: global function  

| Param | Description |
| --- | --- |
| customer_session_key | customer session id value |

**Return**: A dictionary containing some customer info

<a name="getTrainingSheetInfo"></a>
## getTrainingSheetInfo(customer_session_key)
This method allows to get the current Sheet parameters stored inside the cache

**Kind**: global function  

| Param | Description |
| --- | --- |
| customer_session_key | customer session id value |

**Return**: A dictionary containing some current sheet info

<a name="getTodaySheetCustomer"></a>
## getTodaySheetCustomer(customer_key)
This method allows to get the SheetCustomer Django object associated to the current sheet

**Kind**: global function  

| Param | Description |
| --- | --- |
| customer_key | customer DB id value |

**Return**: A SheetCustomer Django object associated to the current sheet

<a name="getTodaySheetCustomer"></a>
## getTrainingSheet(customer_session_key)
This method allows to get the current sheet parameters from the DB

**Kind**: global function  

| Param | Description |
| --- | --- |
| customer_session_key | customer session id value |

**Return**: 

| Var | Description |
| --- | --- |
| completed | customer DB completed value |
| sheet_id | sheet DB id value |
| sheet_name | sheet DB name value |
| duration | sheet DB duration value |
| exercises | dictionary containing the current sheet DB exercises series repetitions, cooldowns and notes |
| exercises_names | dictionary containing the current sheet DB exercises names |

<a name="startSheet"></a>
## startSheet(customer_session_key, date_time)
This method allows to set the current sheet and first exercise states as started inside the cache and DB

**Kind**: global function  

| Param | Description |
| --- | --- |
| customer_key | customer DB id value |
| date_time | session date in String format |

**Return**: A dictionary containing the first exercise first serie repetitions

<a name="startExercise"></a>
## startExercise(customer_session_key, date_time)
This method allows to set the next exercise state as started and the last one as ended inside the cache and DB

**Kind**: global function  

| Param | Description |
| --- | --- |
| customer_key | customer DB id value |
| date_time | session date in String format |

**Return**: A dictionary containing the next exercise first serie repetitions

<a name="skipExercise"></a>
## skipExercise(customer_session_key, date_time)
This method allows to set the next exercise state as started and the last one as skipped inside the cache and DB

**Kind**: global function  

| Param | Description |
| --- | --- |
| customer_key | customer DB id value |
| date_time | session date in String format |

**Return**: A dictionary containing the next exercise first serie repetitions or, if the sheet is just ended, an empty dictionary

<a name="endReps"></a>
## endReps(customer_session_key, date_time)
This method allows to set the next exercise serie as started and the last one as ended inside the cache

**Kind**: global function  

| Param | Description |
| --- | --- |
| customer_key | customer DB id value |
| date_time | session date in String format |

**Return**: A dictionary containing the current exercise next serie repetitions or, if the current exercise is just ended, the next exercise first serie repetitions or, if the sheet is just ended, an empty dictionary

<a name="endSheet"></a>
## endSheet(customer_session_key, date_time)
This method allows to set the next exercise serie as started and the last one as ended inside the cache

**Kind**: global function  

| Param | Description |
| --- | --- |
| customer_key | customer DB id value |
| date_time | session date in String format |

**Return**: None

<a name="returnSheets"></a>
## return_sheets(trainer)
Return the list of the sheets created by the logged trainer

**Kind**: global function  

| Param | Description |
| --- | --- |
| trainer| ID of the logged trainer  |


**Return**: the JSON serialized response of the query

<a name="createSheet"></a>
## create(trainer_id, sheet_name, duration, order, exercise_schema_ids, repetitions, cooldowns, notes)
Add a new Sheet record and the relative exercises in the database

**Kind**: global function  

| Param | Description |
| --- | --- |
| trainer| ID of the logged trainer  |
| sheet_name| Name of the created sheet  |
| duration| Temporal duration of the sheet  |
| order| List containing the order of the exercises  |
| exercise_schema_ids| IDs of the selected ExerciseSchema  |
| repetitions| List containing the number of repetition for each exercise |
| cooldowns| List containing the cooldown time between each repetition  |
| notes| Notes of the sheet |


**Return**: return the ID oh the new sheet, the status of the creation query and the list of the exercise objects




