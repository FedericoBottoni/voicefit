## README:
## This file contains the script that is executed by the GitLab runner to deploy and run any application stored on the repository
## The tags image, variables, services and before_script should not be changed and must stay at the top of the script
## You can configure deploy and test rules for a specific branch

## To deploy your branch on the remote test/prod environment, you have to insert the branch name inside the 'test_deploy'/'prod_deploy' stage (look for the comment that says where) and the custom stages names (more about custom stages later) that execute code on these branches (look for the comment that says where)

## To execute code for your branch on the remote server, you have to use a custom stage. First create a new stage name inside 'stages' (look for the comment that says where). Then follow the instructions at the bottom of the file. Remember that you can create multiple stages for a single branch

image: docker:19.03.1

variables:
  DOCKER_TLS_CERTDIR: "/certs"

services:
  - docker:19.03.1-dind

before_script:
  - docker info
  - apk update
  - apk upgrade
  - apk add bash
  - apk add openssh
  - apk add sshpass
  - chmod +x ./deploy_and_execute/deploy.sh
  - chmod +x ./deploy_and_execute/execute.sh
  - chmod +x ./deploy_and_execute/test.sh

stages:
  - test_deploy
#  - prod_deploy
  ## NEVER put a stage name before 'test_deploy' or 'prod_deploy'
  - rel-test-cache
  - rel-test-back
  - rel-test-front
#  - rel-test-back-test
  ## Insert down here your remote execution stage name
  ## Sintax: - [stage_name]

test_deploy:
  stage: test_deploy
  only:
    refs:
      - rel-test
      ## Add here branch names for which deploy should be executed in the remote test environment
      ## Sintax: - [branch_name]
  script:
    - ENVI="test"
    ## Add code execution custom stages that run on test environment in the following list
    ## Sintax: - SHL="[custom_stage_1] [custom_stage_2] [custom_stage_n]"
    - SHL="rel-test-cache rel-test-back rel-test-front"
    - CONT=$(ls)
    - echo "Starting deploy on ${ENVI} environment"
    - sh ./deploy_and_execute/deploy.sh "$ENVI" "$SHL" "$CI_COMMIT_REF_NAME" "$CI_JOB_STAGE" "$SSH_PASSWD" "$CONT"
    - echo "Ending deploy on ${ENVI} environment"

#prod_deploy:
#  stage: prod_deploy
#  only:
#    refs:
#      - rel-prod
#      ## Add here branch names for which deploy should be executed in the remote production environment
#      ## Sintax: - [branch_name]
#  script:
#    - ENVI="prod"
#    ## Add code execution custom stages that run on production environment in the following list
#    ## Sintax: - SHL="[custom_stage_1] [custom_stage_2] [custom_stage_n]"
#    - SHL="rel-test-cache rel-prod-back rel-prod-front"
#    - CONT=$(ls)
#    - echo "Starting deploy on ${ENVI} environment"
#    - sh ./deploy_and_execute/deploy.sh "$ENVI" "$SHL" "$CI_COMMIT_REF_NAME" "$CI_JOB_STAGE" "$SSH_PASSWD" "$CONT"
#    - echo "Ending deploy on ${ENVI} environment"

rel-test-cache:
  stage: rel-test-cache
  only:
    refs:
      - rel-test
  script:
    - ENVI="test"
    - COMS=$(cat "./deploy_and_execute/${ENVI}_${CI_JOB_STAGE}.txt")
    - echo "Starting commands execution for ${CI_JOB_STAGE} stage on ${ENVI} environment"
    - sh ./deploy_and_execute/execute.sh "$ENVI" "$CI_COMMIT_REF_NAME" "$CI_JOB_STAGE" "$SSH_PASSWD" "$COMS"
    - echo "Ending commands execution for ${CI_JOB_STAGE} stage on ${ENVI} environment"

rel-test-back:
  stage: rel-test-back
  only:
    refs:
      - rel-test
  script:
    - ENVI="test"
    - COMS=$(cat "./deploy_and_execute/${ENVI}_${CI_JOB_STAGE}.txt")
    - echo "Starting commands execution for ${CI_JOB_STAGE} stage on ${ENVI} environment"
    - sh ./deploy_and_execute/execute.sh "$ENVI" "$CI_COMMIT_REF_NAME" "$CI_JOB_STAGE" "$SSH_PASSWD" "$COMS"
    - echo "Ending commands execution for ${CI_JOB_STAGE} stage on ${ENVI} environment"

rel-test-front:
  stage: rel-test-front
  only:
    refs:
      - rel-test
  script:
    - ENVI="test"
    - COMS=$(cat "./deploy_and_execute/${ENVI}_${CI_JOB_STAGE}.txt")
    - echo "Starting commands execution for ${CI_JOB_STAGE} stage on ${ENVI} environment"
    - sh ./deploy_and_execute/execute.sh "$ENVI" "$CI_COMMIT_REF_NAME" "$CI_JOB_STAGE" "$SSH_PASSWD" "$COMS"
    - echo "Ending commands execution for ${CI_JOB_STAGE} stage on ${ENVI} environment"

#rel-test-back-test:
#  stage: rel-test-back-test
#  only:
#    refs:
#      - rel-test
#  script:
#    - ENVI="test"
#    - COMS=$(cat "./deploy_and_execute/${ENVI}_${CI_JOB_STAGE}.txt")
#    - echo "Starting commands execution for ${CI_JOB_STAGE} stage on ${ENVI} environment"
#    - sh ./deploy_and_execute/test.sh "$ENVI" "$CI_COMMIT_REF_NAME" "$SSH_PASSWD" "$COMS"
#    - echo "Ending commands execution for ${CI_JOB_STAGE} stage on ${ENVI} environment"






########## remote execution stage creation tutorial ##########
## Add here the commands you want to execute on the remote server for a specific branch after the deployment.
## Remember that they will all be executed starting from the environment (test/prod) folder the branch has been deployed in.
## Use the following template (change only []):

#[stage_name]:
#  stage: [stage_name]
#  only:
#    refs:
#      - [branch_name]
#  script:
#    ## For [environment] use test/prod
#    - ENVI="[environment]"
#    - COMS=$(cat "./deploy_and_execute/${ENVI}_${CI_JOB_STAGE}.txt")
#    - echo "Starting commands execution for ${CI_JOB_STAGE} stage on ${ENVI} environment"
#    ## Uncomment first following row for execution or second one for test
#    #- sh ./deploy_and_execute/execute.sh "$ENVI" "$CI_COMMIT_REF_NAME" "$CI_JOB_STAGE" "$SSH_PASSWD" "$COMS"
#    #- sh ./deploy_and_execute/test.sh "$ENVI" "$CI_COMMIT_REF_NAME" "$SSH_PASSWD" "$COMS"
#    - echo "Ending commands execution for ${CI_JOB_STAGE} stage on ${ENVI} environment"

## Finally create a file called [environment]_[stage_name].txt inside deploy_and_execute directory containing the commands in the form: [command_1] && [command_2] && [command_n]
##########